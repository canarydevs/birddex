
#### Day 5 - Date: 02/9/2024

##### Tasks/Issues:
- Responsive Landing
##### Progress:
- 25%
##### Next Steps:
- finish converting landing.jsx form using styled components to standard jsx.
##### Blockers:
- I was trying to edit the existing code but I found it more fruitfull to start over.

***
#### Day 4 - Date: 02/8/2024

##### Tasks/Issues:
- Edit users.py and geocode error handling to ensure its in accordance with pydantic.
- Remove unnecessary comments/ console.logs and dead code.
##### Progress:
- Complete

##### Next Steps:
- Stretch goals- Responsive Landing
##### Blockers:
- None

***
#### Day 3 - Date: 02/7/2024

##### Tasks/Issues:
- Assist team in deployment.
- We found a small issue within my unit test which was causing it to fail on main during deployment even though it was passing on the branches and development. The code was edited and passes on deployment
##### Progress:
- Complete

##### Next Steps:
- Polish/ remove dead code/ remove console.logs
##### Blockers:
- None

***

#### Day 2 - Date: 02/6/2024

##### Tasks/Issues:
- Responsive Landing
##### Progress:
- It's not going well at all.
##### Next Steps:
- Seek assistance from seirs
##### Blockers:
- I've managed to get the forms to shift vertically but the overlays refuse to co-operate. I've tried using flex and using media quiries but to no avail. the media quiries won't even take affect and its odd as i tested the new css settings for 480 and 320 screen width directly within the css components but when i plug them into a media query they do nothing.

- After speaking with seirs and Riley I was advised to convert the styled components to standard css and jsx and to put the responsive landing as a strecth goal.

***

#### Day 1 - Date: 02/05/2024

##### Tasks/Issues:
- Complete carousel and dashboard css.
##### Progress:
- Completed carsousel and Card css. Worked with Denny to resolve media sizing issues for dashboard.
##### Next Steps:
- We are creating a dashboard specific card; I will edit the dashboard css accordingly to work seamlessly with the new cards.
- I will also be creating a css file for the birds list page to properly space and center the cards.
##### Blockers:
- None.

***
### Week 17

#### Day 4 - Date: 02/01/2024

##### Tasks/Issues:
- Edit carousel and card css, & complete dashboard css.
##### Progress:
- 80% complete.
##### Next Steps:
- Complete carousel and dashboard css.
##### Blockers:
- None

***
#### Day 3 - Date: 01/31/2024

##### Tasks/Issues:
- Completed card.css and edited card.jsx with modal that pops up when add sighting button is clicked.
##### Progress:
- Complete
##### Next Steps:
- Help with dashboard css
##### Blockers:
- Had some major issues with the buttons moving on other team members computer. Figured out after last edit and push that the issue was a personal setting that caused all font to resize to 14 and thus push the buttons out of place.
***

#### Day 2 - Date: 01/30/2024

##### Tasks/Issues:
- Created card.css
##### Progress:
- 75%
##### Next Steps:
- complete card.css
##### Blockers:
- Bootstrap elements have css built within them and I have been stepping through issues. Very close to having all the bugs worked out.

***
#### Day 1 - Date: 01/29/2024

##### Tasks/Issues:
- Deleted unit test for create users after advisement from Riley. Was instructed to take care of a test for a get method as post method testing tends to be over complicated.
- Created unit test for populate_table(test_birds_populate.py) and test passed.
##### Progress:
- Complete
##### Next Steps:
- Front end development- Landing page.
##### Blockers:
- None
***

### Week 16

#### Day 5 - Date: 01/26/2024

##### Tasks/Issues:
- Created unit test for create users.
##### Progress:
- Test is currently failing.
##### Next Steps:
- Find the issue with the test, fix the issue and get a successful test.
##### Blockers:
- Unit test continues to fail. I've fixed every bug i could find but at this point I'm not sure what I'm missing.

***
#### Day 4 - Date: 01/25/2024

##### Tasks/Issues:
- complete the poller Dockerfile.Dev file
- update Docker-compose.yaml to reflect the addition of the poller.
##### Progress:
- Completed poller Dockerfile.Dev file
- Completed update to Docker-compose.yaml

##### Next Steps:
- Assist Team with creation of unit test
##### Blockers:
- None

***
#### Day 3 - Date: 01/24/2024

##### Tasks/Issues:
- Geocode API README and Poller
##### Progress:
- Completed Geocode API README.
- Completed Poller

##### Next Steps:
- complete the poller Dockerfile.Dev file
- update Docker-compose.yaml to reflect the addition of the poller.
##### Blockers:
- Docker was giving me npm errors on ghi. Finally got things situated just before the end of the day with a computer restart.

***

#### Day 2 - Date: 01/23/2024

##### Tasks/Issues:
- Geocode API
##### Progress:
- Completed Geocode API. The Geocode API uses Google geocoding api for setting the geocoded location of the user which is used to show recently sighted birds on the sit.

##### Next Steps:
- Complete Geocode API README and Poller
##### Blockers:
- None

***
#### Day 1 - Date: 01/22/2024

##### Tasks/Issues:
- Complete Carousel
##### Progress:
- Completed Carousel and have dumby data loaded for proof of concept. Carousel works as intended. Will need to be relocated to the lower end of the webpage later.

##### Next Steps:
- Geocode Api and Poller
##### Blockers:
- None
***

### Week 15

#### Day 5 - Date: 01/19/2024

##### Tasks/Issues:
- Complete Landing(signin/signup) transition function and landing css
- Create issue in gitlab for Carousel completion
##### Progress:
- Completed Landing(signin/signup) transition function and landing css
- Carosusel Issue created in gitlab
- Carousel creation is 75% complete.

##### Next Steps:
- Add carousel to landing
##### Blockers:
- Carousel not functioning. We have a card but not movement of cards.

***

#### Day 4 - Date: 01/18/2024

##### Tasks/Issues:
- Complete Landing(signin/signup) function and landing css

##### Progress:
- 90% complete

##### Next Steps:
- Fix props errors and left/right overlay center and dynamic movement

##### Blockers:
- props

***
### Week 14

***
#### Day 3 - Date: 01/17/2024

##### Tasks/Issues:
- Get method for UsersRepo

##### Progress:
- Complete

##### Next Steps:
- Finish Auth

##### Blockers:
- N/A

***
#### Day 2 - Date: 01/16/2024

##### Tasks/Issues:
- Front end research and build of access page.

##### Progress:
- ran into issues, found a new way to complete the access page.

##### Next Steps:
- rewrite code for access page after class on 01/17/24

##### Blockers:
- none as of now

***
***

### Week 17

#### Day 5 - Date: 02/9/2024

##### Tasks/Issues:

-   Submit final read me and merge to main
-   Final manual testing for bugs/errors

##### Progress:

-   Submitting final journal entry

##### Next Steps:

-   Review with instructor

##### Blockers:

-   None

---

#### Day 4 - Date: 02/8/2024

##### Tasks/Issues:

-   Code cleanup

##### Progress:

-   Fully rebuilt docker volume, images, containers with a successfully

##### Next Steps:

-   Merges

##### Blockers:

-   None

---

#### Day 3 - Date: 02/7/2024

##### Tasks/Issues:

-   (Team) CI/CD

##### Progress:

-   Successfully deployed our DB, Container, Front-end with live URL

##### Next Steps:

-   Code Cleanup

##### Blockers:

-   None

---

#### Day 2 - Date: 02/6/2024

##### Tasks/Issues:

-   Integrate Carousels for sightings and observations
-   UI touchup

##### Progress:

-   Built a carousel componenent with React Bootstrap
-   Works for both sightings and observation API
-   Added interface styling

##### Next Steps:

-   CI/CD

##### Blockers:

-   None

---

#### Day 1 - Date: 02/5/2024

##### Tasks/Issues:

-   Start building out carousel

##### Progress:

-   Skeleton carousel populates with fake data

##### Next Steps:

-   Fix Redux query

##### Blockers:

-   Carousel data

---

### Week 16

#### Day 4 - Date: 02/1/2024

##### Tasks/Issues:

-   Finish create sighting form

##### Progress:

-   Form posts successfully to the sightings api
-   Modal closes after submission
-   Sightings updates realtime

##### Next Steps:

-   Merges

##### Blockers:

-   None

---

#### Day 3 - Date: 01/31/2024

##### Tasks/Issues:

-   Create sightings form react component

##### Progress:

-   Form loads in the modal but post fails

##### Next Steps:

-   Fix submission post error

##### Blockers:

-   Form errors

---

#### Day 2 - Date: 01/30/2024

##### Tasks/Issues:

-   Created Accordion shell

##### Progress:

-   Accordion renders with props data

##### Next Steps:

-   Integrate accordion with dashboard and birds page

##### Blockers:

-   Accordion integration

---

#### Day 1 - Date: 01/29/2024

##### Tasks/Issues:

-   Completed the post call to create a sighting
-   Get all tests merged

##### Progress:

-   Completed tests

##### Next Steps:

-   React Components

##### Blockers:

-   None

---

Week 15

#### Day 5 - Date: 01/26/2024

##### Tasks/Issues:

-   Create Unit Test

##### Progress:

-   Completed unit test for create bird function

##### Next Steps:

-   Frontend kickoff

##### Blockers:

-   None

---

#### Day 4 - Date: 01/25/2024

##### Tasks/Issues:

-   (Team) Finish Backend Merges

##### Progress:

-   Merged all API integrations
-   Poller is working

##### Next Steps:

-   Tests

##### Blockers:

-   None

---

#### Day 3 - Date: 01/24/2024

##### Tasks/Issues:

-   Finish Sightings API

##### Progress:

-   Completed get call for sighting details
-   Completed put call to update sighting
-   Completed delete call to delete sighting

##### Next Steps:

-   Finish merge

##### Blockers:

-   Merge on Gitlab

---

#### Day 2 - Date: 01/23/2024

##### Tasks/Issues:

-   Create Sightings API

##### Progress:

-   Completed get call for single sighting
-   Completed put call to update sighting
-   Completed delete call to delete sighting

##### Next Steps:

-   Finish merge

##### Blockers:

-   Merge on Gitlab

---

#### Day 1 - Date: 01/22/2024

##### Tasks/Issues:

-   Completed the post call to create a sighting
-   Completed the get call to list all sightings for a user

##### Progress:

-   2/5 routes complete

##### Next Steps:

-   Finish Sightings API

##### Blockers:

-   None

---

### Week 14

#### Day 5 - Date: 01/19/2024

##### Tasks/Issues:

-   Create Nav react component, persists across all pages for SPA feel.

##### Progress:

-   Draft Nav component built
-   Reorganized component and route folders for Vite

##### Next Steps:

-   Finish frontend auth

##### Blockers:

-   Redirection after authentication and protected routes

---

#### Day 4 - Date: 01/18/2024

##### Tasks/Issues:

-   (Team) getting vite to work and started
    login component

##### Progress:

-   Vite is working

##### Next Steps:

-   Start frontend auth

##### Blockers:

-   React errors

---

#### Day 3 - Date: 01/17/2024

##### Tasks/Issues:

-   Created auth file for jwtdown implementation

##### Progress:

-   Docker build complete
-   Postgres connected

##### Next Steps:

-   Finish auth

##### Blockers:

-   ***

#### Day 2 - Date: 01/16/2024

##### Tasks/Issues:

-   (Team) Created GIT Workflow

##### Progress:

-   Completed GIT Workflow with team practice

##### Next Steps:

-Docker
-Postgres
-Auth

##### Blockers:

-   None

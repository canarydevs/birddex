# My Project Journal

## Post-Project - Date: 4/22/2024

### Tasks/Issues:

After pushing the queries refactor, the updater kept failing.

### Progress:

I tracked the problem to **SightingsRepo** `get_sightings` and `update_sightings`. The sightings update endpoint was calling `SightingsRepo.update` instead of `SightingsRepo.update_sighting`, and `get_sighting` was passing an empty array into the sql query when called with no arguments.

### Next Steps:

I'm going to fix the seeder before pushing.

### Blockers:

I've cleareed my blockers for now.

***

## Post-Project - Date: 4/2/2024

### Tasks/Issues:

Some of the queries use formatted strings to make sql calls, while psycopg prefers to pass strings with `%s` markers along with a list of data to insert.

There are also other code formatting changes and refactoring that can help the query/enpoint pipeline.

### Progress:

I started this branch four times. In theory, this is all simple updates and janitorial work. The first three times, I made a lot of simple changes and then started testing. There were so many problems that I basically couldn't tell what part was broke and whether any change improved the situation or not. Each time I deleted the branch I made fewer changes before testing. I finally succeeded once I was testing each function immediately after refactoring.

I decided to rename repo methods to include the name of the repo. This makes it easier to find the functions in the future.

### Next Steps:

I've now made significant improvements, but the unit tests are using deprected terms, so I will need to update those before merging.

### Blockers:

I've cleareed my blockers for now.

***

## Post-Project - Date: 3/19/2024

### Tasks/Issues:

Fixing Brian's local project.

### Progress:

I told Brian where to find the update functions to correct the database. However, the api calls failed for `api/birds/update` and `api/birds/update_by_id` returned id is not int error.

The image updaters were supposed to take no arguments. We eventually discovered that they are conflicting with `api/birds`.

### Next Steps:

I should update the updater endpoints to not use `sightings` or `birds` segments.

### Blockers:

Brian tried to do a project demo inside his local Birddex instance, but the images were still in http format.

***

## Post-Project - Date: 02/29/2024

### Tasks/Issues:

I need to write an individual image conversion endpoint.

### Progress:

I wrote an updater that uses an Azure Table to automatically increment the id of the bird. It successfully updated every bird except the Roseate Tern. For some reason, the image comes back None from updates.py 103.

```
    id = index_table.increment()
    bird = bird_repo.get_bird_by_nametype("id", int(id))[0]

    if bird.image[:4] == "http":
        image, content_type = get_image_from_url(bird.image)
```

### Next Steps:

I'm moving on from this for now. I have a refactor issue coming up.

### Blockers:

The update endpoint crashes before updating all images.

***

## Post-Project - Date: 02/28/2024

### Tasks/Issues:

I'm going to rewrite the update system to operate on call instead of at startup.

### Progress:

I added two routes to the api: `/api/birds/update` and `/api/sightings/update`. Each calls the repo.get() then checks each entry's image for "http". If it finds one, it fetches the image, then deposits the image, then updates the record.

Update sightings took two passes, but it worked. Birds worked for about 180 birds, but seems to be stuck there. On the upside, it isn't duplicating images anymore.

I changed the update functions to apply the string "None" if it fails to deposit the image, rather than return the function, but it still won't update the rest of the database.

### Next Steps:

Tomorrow I will write image conversion into a new `PUT /api/birds/` so that when I run PUT, it will update the image.

### Blockers:

The live API gives 502 or 503 error.

***

## Post-Project - Date: 02/27/2024

### Tasks/Issues:

I need to troubleshoot the live API being down.

### Progress:

I inspected the `glv-cloud-cli` command and found the logs for the api. It reads "Aplication Starting up...", so I left it to see if it would complete startup. After hours, it was still stuck, so I checked the Blob Storage Container, and noticed that we have thousands of images stored. This led me to the realization that the program was successfully creating the image, but failing to deposit, and that failure was resetting the server, which causes the seeder to run again.

### Next Steps:

I'm going to rewrite the update system to operate on call instead of at startup.

### Blockers:

The live API gives 502 or 503 error.

***

## Post-Project - Date: 02/26/2024

### Tasks/Issues:

I have a MR in for my blob-storage branch. While I await testing, I plan to run the seeder on a fresh database.

### Progress:

The update got hung up on the fresh database. I eventually narrowed the problem to Pillow limiting the size of images. To correct this, I overrode `Image.MAX_IMAGE_PIXELS`.

Phil tested the update and gave the thumbs up to merge, but I got the same hang up issue.

### Next Steps:

I need to bebug and get the live service running.

### Blockers:

None

***

## Post-Project - Date: 02/23/2024

### Tasks/Issues:

I need to write backend tests for `BlobQueries` and then create a Merge Request.

### Progress:

I struggled with mocking, but as with most problems, I was overthinking it. Once I made a procedural chain of mocks starting with the first db connection I was able to quickly test the `list_blob` and `get_bird_image` functions.

I needed to mock two things to test the `deposit_bird_image` function. I looked into `patch.multiple`, but I only needed to provide two decorators. Simple.

### Next Steps:

Merge to main, reboot live api. Wait for the seeder to work.

### Blockers:

I need to understand mocking better. The BlobQueries don't have a database dependency, but I can't allow the test to run on the database.

***

## Post-Project - Date: 02/22/2024

### Tasks/Issues:

I have a branch with active blob storage and api. I need to update the frontend to accept files and store them on the blob.

### Progress:

I used the unwrap() method on the RTK Mutation that posts the image. The sighting mutator then awaits the resolution on the image mutator.

_I made the requests, but the body was being delviered as a string. Turns out that RTK Query always JSONifies its bodies. I had to change the baseQuery so that if `imageInput` is in the body, it bypasses the JSONify step._

Once I worked out the upload process, I changed the <img> tags to pull images from our `/api/image/`.

### Next Steps:

Write backend tests and create MR.

### Blockers:

-   I've set up a blob storage account, and I have the API endpoints working so that I can submit and retrieve image files by api. I tried to update the other apis to accept image data in addition to json, but I've found out that I can't do that. I have to convert all of our forms to send multipart form data, or I have to make an api call to store the image, then attach the new link to the json data and make another call to store the sighting/bird data. I think I prefer the latter.

    1. Is it reasonable to do this as a two-step process, or will it be frowned upon in a production environment?
    2. If it's reasonable, I imagine I would have handleSubmit() call the image api, and have a useEffect() that calls the addSighting() api when the image api returns a valid link. Is there a better way?

-   **Phil:** The classifier on 38-add-sighting works via a useEffect on the image field of the modal, so I like the idea of creating a valid image url first. <strike>Is there any way doing it in a one or two-step process might affect the ux at all?</strike> Would it matter from a ux perspective at all? If not, I like two step just from a separation of concerns standpoint.

***

## Post-Project - Date: 02/21/2024

### Tasks/Issues:

I have a class to connect to an Azure Storage Account and I need to implement methods to handle the input/output.

### Progress:

I set up data retrieval, but I was having difficulty inputting the image data. Particularly when I tried to access and transform the image using `PIL`, it kept turning black. Phil and Brian helped me out and Phil had the idea to inspect the data going into the crop function. I found out that I had the top and bottom arguments reversed, wich was causing the size to become negative. _Top is always lower than bottom._

### Next Steps:

Work on front end images.

### Blockers:

None

---

## Post-Project - Date: 02/13/2024

### Tasks/Issues:

Instructor feedback included refactoring the Frontend, using Pydantic error handling, and adjusting the api urls to be more RESTful.

### Progress:

I took some time to refactor our frontend components, making the files shorter. For some components it went very well, but for others I wish I hadn't. For one thing, most of the lines of code in the front end are JSX. JSX tags, when formatted for readbility, can be 10 or 15 lines long each, and still comprise a single component. Frequently these files have attached logic, and there's no reason to separate the JSX and the JS. It adds coupling and complexity without increasing modularity or readability.

### Next Steps:

Create blob storage and add the functionality to upload images.

### Blockers:

None

---

## Day 19 - Date: 02/09/2024

### Tasks/Issues:

Today is the last day before grading. This project has been an eye opening experience. I definitely want to remember these experiences and bring what I've learned into a real production environment. In particular, journaling has been incredibly helpful, although some days I spend more time journaling and writing Merge Requests than on writing code! But I have plenty of notes to review and learn from due to this experience. Working with a group has helped me to learn about my strengths and weaknesses in a dev team, and I'm looking forward to do more collaborative work in the future.

We'll merge our last changes and possibly demo today!

### Blockers:

We have polish items to achieve, but MVP is complete and we are ready to demo!

---

## Day 18 - Date: 02/08/2024

### Tasks/Issues:

We are deployed!

### Progress:

_I created a `renderList` function on **Dashboard** to reduce ternary chaining. We have a common pattern, when using fetch data, of checking for existence and error before rendering. Accordion List broke the pattern, so I extracted the last part, and now the pattern in the JSX code is preserved._

```javascript
{
    typeof observationsLoading !== "undefined" ? (
        observationsLoading ? (
            <p>Loading...</p>
        ) : observationsError ? (
            setErr(observationsError)
        ) : observations ? (
            <RBCarousel info={observations} />
        ) : null
    ) : null;
}
```

I created issues for outstanding polish items and stretch goals.

_The live server doesn't allow connections via url except to index._

### Next Steps:

Work on stretch goals and polish the site.

### Blockers:

Styled-components doesn't play well with @media for some reason.<br>
[This may be related](https://stackoverflow.com/questions/72602422/how-to-override-styles-with-higher-specificity-styled-components)

---

## Day 17 - Date: 02/07/2024

### Tasks/Issues:

Open issues are mine connecting **Birds** page with the new birds GET and Brian's adapting the landing page to mobile screens. We have some polish left to do, but MVP is complete after my merge. Possibly starting CI/CD today?

### Progress:

Completed my branch and merged. We spent the morning tweaking the accordion lists and working on the mobile screen css. _Brian spent last night and this morning working on the mobile screen css and was getting frustrated. We advised taking a break to work on something else. When couldn't get the route protection working on day 13, I decided to shelve the idea and move on to more important things._

We got the site working just in time to end the day. _Phil noticed that our data was on the server at root instead of at `/birddex`. So we removed the `birddex/` from `npm run build` and from the `VITE_PUBLIC_URL` and it works now._

### Next Steps:

Stretch goals and fine tuning!

### Blockers:

Styled-components doesn't play well with @media for some reason.<br>
[This may be related](https://stackoverflow.com/questions/72602422/how-to-override-styles-with-higher-specificity-styled-components)

## Day 16 - Date: 02/06/2024

### Tasks/Issues:

We all have merges this morning. Rob needs help with his branch.

### Progress:

Today was very productive and everyone contributed to each section of the project.

Rob was held back by undefined states in API calls, which is an issue we have dealt with earlier. We added the nearby observations API call to the **Dashboard** `useEffect` so it would trigger after user was logged in. Then we rendered the carousels on a ternary that only triggers when `isLoading` exists and is true.

We decided to reimagine the cards in the carousels on **Dashboard**.

I updated the `GET /api/birds` endpoint to provide sightings attached to the bird. Phil and I updated the **Birds** page to use the new data. Phil came up with a streamlined filter process. _I tried to eliminate the need to copy `birds`, but it ended up being a slower process overall, even though it used more memory. I was able to modify Phil's algorithm to hit both birds, though._

```javascript
let filteredBirds = birds.filter((bird) => {
    let keep = true;
    if (filterSightings) keep = keep && bird.sightings.length > 0;
    if (filterSearch)
        keep =
            keep &&
            (bird.common_name
                .toLowerCase()
                .includes(filterSearch.toLowerCase()) ||
                bird.sci_name
                    .toLowerCase()
                    .includes(filterSearch.toLowerCase()));
    return keep;
});
```

_Yesterday, Brian and I discovered that CSS imported to one component affects every component that is currently rendered. CSS is not scope-limited like Javascript/JSX._

### Next Steps:

Something else

### Blockers:

Rob is dealing with undefined API data.

---

## Day 15 - Date: 2/5/2024

### Tasks/Issues:

Today we begin work on the final touches of the project. We are nearly at MVP, and my task is to make the main pages a little friendlier.

### Progress:

Brian and I went through some modifications on the Dashboard including adding header font and centering the hero image.

`Card.jsx` line 46-49: `class="mx-auto my-2"` is what set Bird list's spacing and breaks dashboard's spacing.

```jsx
            <div
                className="card-container mx-auto my-2"
                style={{ width: '17.5rem', height: '20.625rem' }}
            >
                <div className="card">
```

The Birds page is a mess of spaghetti code and needs refactoring. We spent over an hour trying to make it function with Phil's `PaginatedRows` component.

### Next Steps:

Work with the others to get MVP complete and shine the css.

### Blockers:

None

---

## Day 14 - Date: 02/01/2024

### Tasks/Issues:

Identify difs between current state and MVP.

### Progress:

Rob solved the `addSighting` issue. He found out that the arguments to a query in RTK Query need to be a single object. _We aren't calling query directly. RTK Queries take a single argument, which is then passed to the query endpoint._

Rob needed to make the sightings form modal close after submitting a card. I pointed out that he could create a function to do that and pass it as a prop. _I learned recently that passing a function as a prop is a working around to the scope of the component. The passed-in function maintains it's higher scope. I would be wary about passing it down more than once, as that indicates convoluted code._

We identified 4 outstanding issues with our MVP, and assigned them going forward, so that we can be sure to complete the project ASAP. Following that, we'll evaluate deployment and stretch goals.

### Next Steps:

Long weekend tomorrow! I have to get ready for Mer's doctor's appointment and my DL appointment. Maybe some job apps in the meantime.

### Blockers:

None

---

## Day 13 - Date: 01/31/2024

### Tasks/Issues:

I completed the login logic yesterday, and I plan to dive into the Dashboard today.

### Progress:

Loading the sighting list was causing an error when first logging in because it tried to fetch before the user was updated. I was able to catch the error, but it still produced a failed api call in the terminal before updating the data on the page. I finally fixed this using RTK Queries Lazy fetching. Now the user is loaded first, and when the user data is updated, it triggers the sightings call.

_We spent too long trying to redirect unauthorized users away from the Dashboard._<br>
I protected the link, but I wanted to make sure users couldn't type in the url to access Dashboard without being logged in. The problem was the same as with fetching sightings, the user isn't updated on the backend, until after the page has navigated to dashboard and then been kicked back for being unauthorized. Every method we tried seemed to create a loop as the user data gets updated on load, which causes a load, which updates user. In the end, I made rendering the Dashboard conditional on being logged in. Now when the user is redirected there, a welcome page is rendered for miliseconds while the user is logged in, and then updates to the full dashboard. If the logged in user is `null`, then the welcome page remains, which directs the user to return to the Landing page.

We spent the afternoon working through submitting JSON data to our create endpoint. The `formData` state was being used to create an object, but the object refused to be created for some reason.

### Next Steps:

Push this and move on.

### Blockers:

-   Need to figure out why the `addSighting` endpoint is not getting `info` properly.

---

## Day 12 - Date: 01/30/2024

### Tasks/Issues:

We were able to get login working with the userSlice last night so now we need to complete the login logic, and assign responsibilities. I'm going to pitch adding an error slice to the top level.

### Progress:

We made great progress on Birds page, Landing Page, and AccordionList.

_I tried to add Redux without refactoring my existing login logic, but Riley popped into the room and explained that an employer would wonder, "why are you mixing technologies here?" So, I refactored completely to use Redux login._

_We pulled a [function from StackOverflow](https://stackoverflow.com/questions/36862334/get-viewport-window-height-in-reactjs) that creates an event listener and resizes our columns based on the size of the viewport._

### Next Steps:

I plan to start work on the Dashboard in the morning.

### Blockers:

None

---

## Day 11 - Date: 01/29/2024

### Tasks/Issues:

On Friday, I ended up only coding tests for `acl_ebirds` and `ebirds_api`, and I modified some of the expectations of the tests. I also needed to modify the functions that I was testing, and improve their error handling. Over the weekend, I used Redux to implement the list insertion.

Today we need to get started on front end.

### Progress:

We helped Brian finish up, and merged everyone's changes from unit testing. Then we decided on using Redux and began building the `store.js`.

_We were stuck on the Landing page for hours because `dispatch()` was receiving an undefined action. We tried console.logging all the data, and everything said it should be working. We finally noticed that the line `import setId from '...'` should have been `import { setId } from '...'` after changing that, our page was up and ready to go. Somtimes it's the little things..._

### Next Steps:

Next we need to complete updating the login logic and

### Blockers:

None at this time.

## Day 10 - Date: 01/26/2024

### Tasks/Issues:

The backend was substantively completed yesterday and we only have to merge in Phil's data seeder. Today, we'll work on testing, and next week start on front-end work.

### Backend Testing Design

**acl.ebirds** ->

-   Given 3 eBirds observations, and 3 `birds` in the database with matching "common*name", *`get_obs()` should return a **List** of **Observations** with length 3, all with images\_
-   Given 3 eBirds observations, and 1 `birds` in the database with matching "common*name", *`get_obs()` should return a **List** of **Observations** with length 3, one with image\_
-   Given 2 eBirds observations, and no `birds` in the database with matching "common*name", *`get_obs()` should return a **List** of **Observations** with length 2, all without images\_
-   Given eBirds fetch failed, _`get_obs()` should return None_

**routers.ebirds_api ->**

-   Given 3 eBirds observations, and 3 `birds` with matching "common*name", *`get_obs_US()` should return a **List** of **Observations** with length 3, all with images\_
-   Given 3 eBirds observations, and 1 `birds` with matching "common*name", *`get_obs_US()` should return a **List** of **Observations** with length 3, one with image\_
-   Given 2 eBirds observations, and no `birds` with matching "common*name", *`get_obs_US()` should return a **List** of **Observations** with length 2, all without images\_
-   Given ebirds fetch failed, _`get_obs()` should return `null`_

**acl.nuthatch ->**

-   _`get_birds()` should return a **List** of **BirdIns** with length 100_
-   _`get_birds(2)` should return a **List** of **BirdIns** with length 100_
-   _first list should be different **BirdIns** from second list_
-   _`get_birds(page_size=25)` should return a **List** of **BirdIns** with length 25_

**queries.users ->**

-   Given a user email that exists, _`UsersRepo.get()` should return the user data_
-   Given a user email that doesn't exist, _`UsersRepo.get()` should return None_

**routers.users ->**

-   [ ] Given a valid user account, _`create_account()` should log the user in and return_ json including a bearer token along with the account info
-   [ ] Given a valid input for an email that already exists, _`create_account()` should raise HTTPException_

### Progress:

The team completed 3 backend tests today. I've validated the eBirds endpoints and they do return a 200 response when called. Instructor Dalonte pointed out that for our api call, all I need to know is that I can get a 200 response. What's in the response doesn't matter in this function. I need to validate the acl function as well, using Mocker. It was difficult to parse the methods of `unittest.Mock`, but I was able to get it eventually.

_`Mock` is capable of replacing a `requests.Response` object. I do have to make sure to assign the expected result to the .text or .json property as needed in the tested function._

### Next Steps:

Monday we plan the frontend.

### Blockers:

Learning unit testing, especially for APIs and databases

---

## Day 9 - Date: 01/25/2024

### Tasks/Issues:

Today we need to finish the poller and design backend tests.

### Progress:

Brian completed the poller today and Phil added a seeder that uses `asyncio` to add to the library 10 seconds after the `fastapi` container launches. I was able to mock-up a Redux-implementing login/logout scheme. We then changed the poller to claim all of the Nuthatch data once per day. We went with Phil's implementation using a loop to schedule 4 events.

'''python
for i in range(1,5):
schedule.every().day().do(get_bird_data(i))
'''

The alternative solution I came up with is probably over-engineered, but I was happy to find a pythonic use for anonymous functions:

'''python
pg_num = 1
schedule.every(5).seconds.do(lambda: get_bird_data(pg_num))

    while True:
        schedule.run_pending()
        time.sleep(1)
        pg_num += 1

        if pg_num > 4:
            pg_num = 1
            time.sleep(360 * 24)

'''

The last few items on the Nuthatch list are not valid, so we had to cut the list off at 300, or add logic to skip those:

'''python
if not (
bird.get("status")
and bird.get("images")
and bird.get("name")
and bird.get("sciName")
):
continue
'''

_Yesterday we noticed that the database serial number increases even on a failed `INSERT`. I searched the topic and found out [that is by design.](https://stackoverflow.com/questions/35849944/postgresql-serial-incremented-on-failed-constraint-insert)_

_We decided to use `ENV PYTHONUNBUFFERED1` on the development poller container._

### Next Steps:

Tomorrow we work on unit testing and finish up backend

### Blockers:

-   None

---

## Day 8 - Date: 01/24/2024

### Tasks/Issues:

Last night I drafted merge requests for Nuthatch API and eBirds API.

Today we will spend time consolidating and propagating our work so far.

### Progress:

We discussed the process of handling the several merge requests in the pipeline, and then followed through by merging each of the live branches.

_Reviewing the Microservices Two Shot project, I learned that the poller was simply running in it's own Python environment without a server. We should be able to set one up to hit the GET /api/birds/
populate endpoint and need no logic of it's own._

### Next Steps:

Finish the poller with Brian and move on to front end.

### Blockers:

anything

---

## Day 7 - Date: 01/23/2024

### Tasks/Issues:

Yesterday I finished my own frontend Auth implementation. _The exploration did it differently, and I need to find out if there is good reason to change it._ Last night, I researched and practiced using Redux. I really like it, and need to help the group decide if we need it. I don't think we will need it for data management, but will need it for state management, so Cards can change the state of Dashboard and Birds.

Today I need to get to work on Nuthatch API.

### Progress:

I was able to finish NuthatchAPI and eBirds API calls today. We had significant setbacks with Geocode API. _We found out that when there is a validation error or a account already exists error, it manifests as a CORS error in the browser._ I created a formatted string to encode the Nuthatch API call, but on the eBirds API, I remebered that I can enter `params` as an argument to `requests.get()`.

### Next Steps:

We will need to test and merge everyone's branches.

### Blockers:

-   Need to work on our workflow
-   Need CI/CD training and testing training

---

## Day 6 - Date: 01/22/2024

### Tasks/Issues:

On Friday I stayed up late researching front end auth and I discovered the solution. I'm ready to correct Landing.jsx. Over the weeekend I reached out to a contact in UX design for advice, then went on to write a proof of concept for our bird cards/accordion list.

Today I want to finish the Setup Vite issue, then work on diagramming the logic of the front end data. This is critical to deciding if we need RDX.

### Progress:

Phil and I merged today, so the app now has a basic frontend auth and a birds API.

I helped Brian today create the API for Google Geocode. We had previously used headers or JSON to pass data to an API, so we talked about the difference when handling calls using queries.

### Next Steps:

Tomorrow I will begin work on the Nuthatch API.

### Blockers:

-   Need to work on our workflow
-   Need to create issues and divide the work
-   Need CI/CD training and testing training

---

## Day 5 - Date: 01/19/2024

### Tasks/Issues:

I completed the JWT implementation docs and submitted the MR last night. My app is running again after a reclone from main.

I didn't get any coding in yesterday, but we should be able to start pairing today.

### Progress:

I added login and signup logic to the landing page today. We still need to handle errors and perform redirects.

We divvied up some of the outstanding issues. I created an issue to complete error handling.

_I was able to use simple if/else logic to provide a single function to sign in or sign up. I discovered that the POST /token api doesn't take a json, its a `x-www-form-urlencoded`, so we'll need to update the README._

_During after-hours reasearch, I learned that I should use `credentials: 'include'` in the fetchConfig so that the login token can be accessed. I skipped the heading "Getting tokens from HTTP-only cookies" on the JWTdown guide, planning to return later. That ended up creating lots of wasted time._

### Next Steps:

Complete redirects and move on.

### Blockers:

Need to create issues and divide the work. Need CI/CD training and testing training.

---

## Day 4 - Date: 01/18/2024

### Tasks/Issues:

We're using "user" to mean "user login account" and "bird" to mean "a specific species of bird". Should we create a universal language section in the Readme?

I have merge requests in the pipeline today and we start Landing page frontend work.

### Progress:

We started off planning to complete the landing page including login/logout logic today. I suggested we use mob programming on this step to get everyone's feet wet in frontend. We were able to create the frontend landing page, but still need to implement api calls and complete the css.

I suggested we cancel the sprint and complete the current issue, then begin pair programming tomorrow.

As the end of the night came around, my birddex project failed. npm repeatedly failed at updating when starting the frontend app, apparently due to already existing files. I tried deleting the node_modules folder and restarting Docker, but eventually I had to delete my local repository and reclone.

_You need a space between variable and data in YAML!_

### Next Steps:

Finish Login page and separate into pairs.

### Blockers:

Need CI/CD training and testing training.

---

## Day 3 - Date: 01/17/2024

### Tasks/Issues:

Today we start the JWTdown backend work. Mob programming to complete the process.

### Progress:

I added the birddex-db to our project. There was a stumbling block trying to get the DATABASE_URL working. Once we identified the proper format, it initialized perfectly.

### Next Steps:

Finish Auth

### Blockers:

None at the moment

---

## Day 2 - Date: 01/16/2024

### Tasks/Issues:

Create and lay the groundwork for Project Gamma.

### Progress:

Completed JWTdown tutorial. Started Backend issues and made a roadmap for the backend build.

_I noticed that I can pass back an AccountOutWithPassword to the get(username) function, and the caller can turn it into an AccountOut if needed._

### Next Steps:

Start the JWTdown backend.

### Blockers:

None at the moment.

---

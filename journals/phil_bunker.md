***
***
### Week 14

***
#### Day 4 - Date: 01/18/2024

##### Tasks/Issues:
-

##### Progress:
-

##### Next Steps:
-

##### Blockers:
-

***
#### Day 3 - Date: 01/17/2024

##### Tasks/Issues:
- Assist w/ Jwtdown implementation

##### Progress:
- Following Denny's lead, help add API endpoints for user auth
  - Created API's for GET, POST and DELETE at /token
  - Created API for POST at /api/users
  - Created 001_create_users_table migrations table

##### Next Steps:
- Finish auth

##### Blockers:
- None

***
#### Day 2 - Date: 01/16/2024

##### Tasks/Issues:
- Create journal template format

##### Progress:
- Journal templates added and complete

##### Next Steps:
- Assist in JWTdown implementation

##### Blockers:
- None

***
***

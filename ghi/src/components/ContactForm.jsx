function ContactForm() {
    return (
        <>
            <button
                type="button"
                className="nav-link"
                data-bs-toggle="modal"
                data-bs-target="#contact-modal"
            >
                Contact
            </button>
            <div
                className="modal fade"
                id="contact-modal"
                role="dialog"
                aria-labelledby="contact-modalLabel"
                aria-hidden="true"
            >
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h1
                                className="modal-title fs-5"
                                id="contact-modalLabel"
                            >
                                Contact Info
                            </h1>
                            <button
                                type="button"
                                className="btn-close"
                                data-bs-dismiss="modal"
                                aria-label="Close"
                            ></button>
                        </div>
                        <div className="modal-body">
                            <h3>Webmaster Email</h3>
                            <ul>
                                <li>
                                    <a href="mailto:dennis.bucklin@gmail.com">
                                        dennis.bucklin@gmail.com
                                    </a>
                                </li>
                            </ul>
                            <h3>Sites</h3>
                            <ul>
                                <li>
                                    <a
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        href="https://gitlab.com/canarydevs/birddex"
                                    >
                                        Birddex Gitlab Repo
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div className="modal-footer">
                            <button
                                type="button"
                                className="btn"
                                data-bs-dismiss="modal"
                            >
                                Close
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default ContactForm;

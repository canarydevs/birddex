import { setListInitialState } from '../listPosition/listPositionSlice'
import { setSelectedInitialState } from '../selectedCard/selectedCardSlice'
import ReactPaginate from 'react-paginate'
import { useState } from 'react'
import { useDispatch } from 'react-redux'

function Rows({ currentRows }) {
    return <>{currentRows && currentRows.map((row) => row)}</>
}

export default function PaginatedRows({ rowsPerPage, rows, currentPage, setCurrentPage }) {
    const [rowOffset, setRowOffset] = useState(0)

    const endOffset = rowOffset + rowsPerPage
    const currentRows = rows.slice(rowOffset, endOffset)

    const dispatch = useDispatch()

    const handlePageChange = (event) => {
        const newOffset = (event.selected * rowsPerPage) % rows.length
        setRowOffset(newOffset)
        // Close the list and unselect cards
        dispatch(setListInitialState())
        dispatch(setSelectedInitialState())
        setCurrentPage(event.selected)
    }

    return (
        <>
            <Rows currentRows={currentRows} />
            <div className={'d-flex justify-content-center py-5'}>
                <ReactPaginate
                    pageCount={Math.ceil(rows.length / rowsPerPage)}
                    pageRangeDisplayed={5}
                    marginPagesDisplayed={1}
                    onPageChange={handlePageChange}
                    forcePage={currentPage}
                    containerClassName={'pagination'}
                    activeClassName={'active'}
                    pageClassName={'page-item'}
                    pageLinkClassName={'page-link'}
                    previousClassName={'page-item'}
                    previousLinkClassName={'page-link'}
                    nextClassName={'page-item'}
                    nextLinkClassName={'page-link'}
                    breakClassName={'page-item'}
                    breakLinkClassName={'page-link'}
                />
            </div>
        </>
    )
}

import { useState } from 'react'
import { Modal } from 'react-bootstrap'

import SightingForm from '../SightingForm'

function BirdsListAccordion(props) {
    const bird = props.info.bird
    const [sightingInfoUpdate, setSightingInfoUpdate] = useState(false)
    const [sightingInfoDelete, setSightingInfoDelete] = useState(false)

    const toggleEditSighting = () => {
        sightingInfoUpdate ? setSightingInfoUpdate(false) : 0
        sightingInfoDelete ? setSightingInfoDelete(false) : 0
    }

    const sortByDate = (sightings) =>
        sightings.sort(({ date: a }, { date: b }) =>
            a < b ? 1 : a > b ? -1 : 0
        )

    const sortedSightings = sortByDate([...props.info.sightings])

    return (
        <>
            <div className="accordion" id="accordion">
                {sortedSightings.map((sighting) => {
                    return (
                        <div className="accordion-item" key={sighting.id}>
                            <h2 className="accordion-header">
                                <button
                                    className="accordion-button collapsed"
                                    type="button"
                                    data-bs-toggle="collapse"
                                    data-bs-target={'#item' + sighting.id}
                                    aria-expanded="true"
                                    aria-controls={'item' + sighting.id}
                                >
                                    {sighting.date}, {sighting.location}
                                </button>
                            </h2>
                            <div
                                id={'item' + sighting.id}
                                className="accordion-collapse collapse"
                                data-bs-parent="#accordion"
                            >
                                <div className="accordion-body row">
                                    <div className="col --bs-body-bg">
                                        <img
                                            src={(
                                                sighting.image &&
                                                sighting.image.length
                                                    ? sighting.image
                                                    : bird.image
                                            )}
                                            className="card-img-top"
                                            loading="lazy"
                                            alt={bird.common_name}
                                            style={{
                                                objectFit: 'contain',
                                                height: '10rem',
                                            }}
                                        />
                                    </div>
                                    <div className="col --bs-body-bg">
                                        <ul className="list-group">
                                            <li className="list-group-item">
                                                Location: {sighting.location}
                                            </li>
                                            <li className="list-group-item">
                                                Latitude: {sighting.lat}
                                            </li>
                                            <li className="list-group-item">
                                                Longitude: {sighting.lng}
                                            </li>
                                            <li className="list-group-item">
                                                Date: {sighting.date}
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col --bs-body-bg">
                                        <button
                                            style={{
                                                marginTop: '.3125rem',
                                                marginBottom: '.3125rem',
                                            }}
                                            className="btn btn-success custom px-1 py-1 mx-2 "
                                            type="button"
                                            onClick={() =>
                                                setSightingInfoUpdate(sighting)
                                            }
                                        >
                                            Update Sighting
                                        </button>
                                        <button
                                            className="btn btn-outline-danger custom px-1 py-1 mx-2"
                                            type="button"
                                            onClick={() =>
                                                setSightingInfoDelete(sighting)
                                            }
                                        >
                                            Delete Sighting
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </div>
            <Modal
                size="md"
                show={sightingInfoUpdate}
                onHide={() => toggleEditSighting()}
                backdrop="static"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="example-modal-sizes-title-sm">
                        Update {bird && (bird.common_name || '')} sighting
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <SightingForm
                        action="update"
                        sightingInfo={sightingInfoUpdate}
                        birdInfo={bird}
                        toggle={toggleEditSighting}
                    />
                </Modal.Body>
            </Modal>
            <Modal
                size="md"
                show={sightingInfoDelete}
                onHide={() => toggleEditSighting()}
                backdrop="static"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="example-modal-sizes-title-sm">
                        Delete {bird && (bird.common_name || '')} sighting
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <SightingForm
                        action="delete"
                        sightingInfo={sightingInfoDelete}
                        birdInfo={bird}
                        toggle={toggleEditSighting}
                    />
                </Modal.Body>
            </Modal>
        </>
    )
}

export default BirdsListAccordion

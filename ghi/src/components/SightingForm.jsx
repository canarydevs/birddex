import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { setListPosition } from './listPosition/listPositionSlice.js'
import {
    useAddImageMutation,
    useAddSightingMutation,
    useUpdateSightingMutation,
    useDeleteSightingMutation,
} from '../components/api/birddexApi.js'
import { setSelectedCard } from './selectedCard/selectedCardSlice.js'

function SightingForm({
    action = '',
    sightingInfo = '',
    birdInfo = '',
    toggle,
}) {
    // holds any active error message
    const [err, setErr] = useState(null)
    const dispatch = useDispatch()
    // RTK Query mutations
    const [addSighting] = useAddSightingMutation()
    const [addImage] = useAddImageMutation()
    const [updateSighting] = useUpdateSightingMutation()
    const [deleteSighting] = useDeleteSightingMutation()
    // initial form data state
    const [formData, setFormData] = useState({
        date: sightingInfo.date || '',
        location: sightingInfo.location || '',
    })

    // convert date data for JSON
    const formatDate = (date) => {
        const isoString = new Date(date).toISOString()
        return isoString.split('T')[0]
    }

    // handle form submit
    const handleSubmit = async (e) => {
        e.preventDefault()

        let image_id = null
        if (e.target[2].value) {
            try {
                const result = await addImage({
                    imageInput: e.target[2].files[0],
                    image_id: formData.location,
                }).unwrap()
                image_id = import.meta.env.VITE_BACKEND_HOST + "/api/images/" + result.blob_name
            } catch (error) {
                setErr(error.message)
            }
        } else {
            image_id = sightingInfo.image ? sightingInfo.image : birdInfo.image
        }

        try {
            switch (action) {
                case 'create':
                    addSighting({
                        info: {
                            bird_id: birdInfo.id,
                            date: formatDate(formData.date),
                            location: formData.location,
                            image: image_id,
                        },
                    })
                    break
                case 'update':
                    updateSighting({
                        sightingId: sightingInfo.id,
                        info: {
                            bird_id: birdInfo.id,
                            date: formatDate(formData.date),
                            location: formData.location,
                            image: image_id,
                        },
                    })
                    break
                case 'delete':
                    deleteSighting({
                        sightingId: sightingInfo.id,
                    })
                    break
            }
        } catch (error) {
            setErr(error.message)
        }

        // Closes the modal
        if (!err) toggle()

        // Closes the accordion so the list can update
        dispatch(setListPosition(-1))
        dispatch(setSelectedCard(-1))
    }

    // updates form data state
    const handleChange = (e) => {
        const value = e.target.value
        const inputName = e.target.name
        setFormData({
            ...formData,
            [inputName]: value,
        })
    }
    return (
        <div className="row">
            <div className="offset-1 col-10">
                <div className="shadow p-4 mt-4 mb-4">
                    <form onSubmit={handleSubmit} id="create-sighting-form">
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleChange}
                                value={formData.date}
                                placeholder="Date"
                                required
                                type="date"
                                name="date"
                                id="date"
                                className="form-control"
                                disabled={action === 'delete'}
                            />
                            <label htmlFor="date">Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleChange}
                                value={formData.location}
                                placeholder="Location"
                                required
                                type="text"
                                name="location"
                                id="location"
                                className="form-control"
                                disabled={action === 'delete'}
                            />
                            <label htmlFor="location">Location</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                placeholder="Image"
                                type="file"
                                accept="image/*"
                                name="image"
                                id="image"
                                className="form-control"
                                disabled={action === 'delete'}
                            />
                            <label htmlFor="image">Image</label>
                        </div>
                        <div className="d-grid gap-2 col-12 mx-auto">
                            <button className="btn btn-success">
                                <b>
                                    {action.charAt(0).toUpperCase() +
                                        action.slice(1)}{' '}
                                    Sighting
                                </b>
                            </button>
                            {err ? (
                                <div
                                    className={
                                        'alert alert-danger alert-dismissible'
                                    }
                                    role="alert"
                                >
                                    {err}
                                </div>
                            ) : null}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default SightingForm

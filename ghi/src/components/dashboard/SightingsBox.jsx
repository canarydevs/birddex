import Accordion from './Accordion'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'

// function to abstract rendering the accordion list
function renderList(sightings, orderSelection, filterSearch) {
    if (typeof sightings === 'undefined') return null

    // filters the sightings list
    let sortedSightings = sightings.filter((sighting) => {
        let keep = true
        if (filterSearch)
            keep =
                keep &&
                (sighting.bird.common_name
                    .toLowerCase()
                    .includes(filterSearch.toLowerCase()) ||
                    sighting.bird.sci_name
                        .toLowerCase()
                        .includes(filterSearch.toLowerCase()) ||
                    sighting.location
                        .toLowerCase()
                        .includes(filterSearch.toLowerCase()))
        return keep
    })
    if (sortedSightings && sortedSightings.length > 0) {
        switch (orderSelection) {
            case 'date-order':
                sortedSightings.sort(({ date: a }, { date: b }) =>
                    a < b ? 1 : a > b ? -1 : 0
                )
                break
            case 'common-order':
                sortedSightings.sort(({bird: a}, {bird:b}) =>
                    a.common_name
                        .toLowerCase()
                        .localeCompare(b.common_name.toLowerCase())
                )
                break
            case 'sci-order':
                sortedSightings.sort(({bird: a}, {bird: b}) =>
                    a.sci_name
                        .toLowerCase()
                        .localeCompare(b.sci_name.toLowerCase())
                )
                break
        }
        return <Accordion info={sortedSightings} />
    } else {
        return (
            <h4 className="m-3">
                You currently have no sightings. Get out there and see some
                birds!
            </h4>
        )
    }
}

export default function SightingsBox({ sightings }) {
    const navigate = useNavigate()
    // holds the list order state
    const [orderSelection, setOrderSelection] = useState('date-order')
    // holds the filter state
    const [filterSearch, setFilterSearch] = useState('')

    // handle the order select dropdown
    const handleOrderSelect = (e) => {
        setOrderSelection(e.target.value)
    }
    // handle the search field
    const handleSearchChange = (e) => {
        setFilterSearch(e.target.value)
    }
    return (
        <div className="mb-5" id="sightings-box">
            <div className="d-flex justify-content-evenly">
                <div className="col justify-content-start">
                    <h3 className="py-2">All Sightings:</h3>
                </div>
                <div className="col justify-content-end pb-2">
                    <div className="row">
                        <select
                            onChange={handleOrderSelect}
                            value={orderSelection}
                            required
                            id="order"
                            name="order"
                            className="col form-select"
                        >
                            <option value="date-order" key="date-order">
                                Recent sighting
                            </option>
                            <option value="common-order" key="common-order">
                                Common Name - A-Z
                            </option>
                            <option value="sci-order" key="sci-order">
                                Scientific Name - A-Z
                            </option>
                        </select>
                        <input
                            type="text"
                            placeholder="Search..."
                            id="searchFilter"
                            onChange={handleSearchChange}
                            value={filterSearch}
                            className="col search-bar m-1"
                        />
                        <button
                            className="col btn btn-success btn-lg px-3"
                            onClick={() => navigate('/birds')}
                        >
                            + Add Sighting
                        </button>
                    </div>
                </div>
            </div>
            {renderList(sightings, orderSelection, filterSearch)}
        </div>
    )
}

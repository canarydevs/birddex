import { useState } from 'react'
import { Modal } from 'react-bootstrap'

import SightingForm from '../SightingForm'

function Accordion(props) {
    const [sightingInfoUpdate, setSightingInfoUpdate] = useState(false)
    const [sightingInfoDelete, setSightingInfoDelete] = useState(false)

    const toggleEditSighting = () => {
        sightingInfoUpdate ? setSightingInfoUpdate(false) : 0
        sightingInfoDelete ? setSightingInfoDelete(false) : 0
    }

    return (
        <>
            <div className="accordion" id="accordion">
                {props.info.map((sighting) => {
                    return (
                        <div className="accordion-item" key={sighting.id}>
                            <h2 className="accordion-header">
                                <button
                                    className="accordion-button collapsed"
                                    type="button"
                                    data-bs-toggle="collapse"
                                    data-bs-target={'#item' + sighting.id}
                                    aria-expanded="true"
                                    aria-controls={'item' + sighting.id}
                                >
                                    {sighting.date}, {sighting.bird.common_name}
                                </button>
                            </h2>
                            <div
                                id={'item' + sighting.id}
                                className="accordion-collapse collapse"
                                data-bs-parent="#accordion"
                            >
                                <div className="accordion-body row">
                                    <div className="col --bs-body-bg">
                                        <img
                                            src={(
                                                sighting.image &&
                                                sighting.image.length
                                                    ? sighting.image
                                                    : sighting.bird.image
                                            )}
                                            className="card-img-top"
                                            loading="lazy"
                                            alt={sighting.bird.common_name}
                                            style={{
                                                objectFit: 'contain',
                                                height: '10rem',
                                            }}
                                        />
                                    </div>
                                    <div className="col --bs-body-bg">
                                        <ul className="list-group">
                                            <li className="list-group-item">
                                                Scientific Name:{' '}
                                                {sighting.bird.sci_name}
                                            </li>
                                            <li className="list-group-item">
                                                Location: {sighting.location}
                                            </li>
                                            <li className="list-group-item">
                                                Latitude: {sighting.lat}
                                            </li>
                                            <li className="list-group-item">
                                                Longitude: {sighting.lng}
                                            </li>
                                            <li className="list-group-item">
                                                Date: {sighting.date}
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col --bs-body-bg">
                                        <button
                                            type="button"
                                            style={{ marginTop: '.3125rem', marginBottom: '.3125rem'}}
                                            className="btn btn-success px-1 py-1
                                            custom mx-2" onClick=
                                            {() =>
                                                setSightingInfoUpdate(sighting)
                                            }
                                            > Update Sighting
                                        </button>
                                        <button
                                            type="button"
                                            className="btn btn-outline-danger px-1 py-1 custom mx-2"
                                            onClick={() =>
                                                setSightingInfoDelete(sighting)
                                            }
                                        >
                                            Delete Sighting
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </div>

            {/* Update Modal */}
            <Modal
                size="md"
                show={sightingInfoUpdate}
                onHide={() => toggleEditSighting()}
                backdrop="static"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="example-modal-sizes-title-sm">
                        Update{' '}
                        {sightingInfoUpdate.bird &&
                            (sightingInfoUpdate.bird.common_name || '')}{' '}
                        sighting
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <SightingForm
                        action="update"
                        sightingInfo={sightingInfoUpdate}
                        birdInfo={sightingInfoUpdate.bird}
                        toggle={toggleEditSighting}
                    />
                </Modal.Body>
            </Modal>

            {/* Delete Modal */}
            <Modal
                size="md"
                show={sightingInfoDelete}
                onHide={() => toggleEditSighting()}
                backdrop="static"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="example-modal-sizes-title-sm">
                        Delete{' '}
                        {sightingInfoDelete.bird &&
                            (sightingInfoDelete.bird.common_name || '')}{' '}
                        sighting
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <SightingForm
                        action="delete"
                        sightingInfo={sightingInfoDelete}
                        birdInfo={sightingInfoDelete.bird}
                        toggle={toggleEditSighting}
                    />
                </Modal.Body>
            </Modal>
        </>
    )
}

export default Accordion

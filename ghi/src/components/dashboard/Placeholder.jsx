import { useNavigate } from "react-router-dom";

export default function Placeholder({ err }) {
    const navigate = useNavigate();

    return (
        <>
            <h1>Birddex</h1>
            <div className="row justify-content-center">
                <div className="col text-center">
                    <img
                        className="hero-image"
                        id="hero"
                        src="\flock-of-flying-birds-vector.png"
                        alt="Flock of flying birds"
                    />
                </div>
                <div className="text-center">
                    <button
                        className="btn btn-success large px-5 py-3 mt-5"
                        onClick={() => navigate('/')}
                    >
                        Sign In
                    </button>
                </div>
            </div>
            <div>
                {err ? (
                    <div
                        className={
                            'alert alert-danger alert-dismissible'
                        }
                        role="alert"
                    >
                        {err}
                    </div>
                ) : null}
            </div>
        </>
    )
}

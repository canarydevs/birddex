import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const birdsApi = createApi({
    reducerPath: 'birddexApi',
    tagTypes: ['Birds', 'Sightings', 'User'],
    baseQuery: (args, api, extraOptions) => {
        let body = args.body
        if (body && body.imageInput) {
            // Don't stringify FormData
            args.body = undefined
        }
        const result = fetchBaseQuery({
            baseUrl: `${import.meta.env.VITE_BACKEND_HOST}`,
        })(args, api, extraOptions)
        if (body && body.imageInput) {
            // Add the FormData to the request manually
            result.promise = result.promise.then((response) => {
                response.request.body = body
                return response
            })
        }
        return result
    },
    endpoints: (builder) => ({
        getBirds: builder.query({
            query: () => ({
                url: '/api/birds',
                credentials: 'include',
            }),
            providesTags: ['Birds'],
        }),
        getSightings: builder.query({
            query: () => ({
                url: `/api/sightings`,
                credentials: 'include',
            }),
            providesTags: ['Sightings'],
        }),
        getToken: builder.query({
            query: () => ({
                url: '/token',
                credentials: 'include',
            }),
            providesTags: ['User'],
        }),
        getObservations: builder.query({
            query: () => ({
                url: `/ebirds/nearby`,
                credentials: 'include',
            }),
            providesTags: ['Observations'],
        }),
        login: builder.mutation({
            query: (info) => {
                let formData = new FormData()
                formData.append('username', info.email)
                formData.append('password', info.password)

                return {
                    url: '/token',
                    method: 'POST',
                    body: formData,
                    credentials: 'include',
                }
            },
            invalidatesTags: ['User', 'Birds'],
        }),
        logout: builder.mutation({
            query: () => ({
                url: '/token',
                method: 'DELETE',
                credentials: 'include',
            }),
            invalidatesTags: ['User', 'Birds'],
        }),
        signup: builder.mutation({
            query: (info) => {
                return {
                    url: '/api/users',
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(info),
                    credentials: 'include',
                }
            },
            invalidatesTags: (result) => {
                return (result && ['User', 'Birds']) || []
            },
        }),
        addSighting: builder.mutation({
            query: ({ info }) => {
                return {
                    url: `/api/sightings/`,
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(info),
                    credentials: 'include',
                }
            },
            invalidatesTags: (result) => {
                return (result && ['Sightings', 'Birds']) || []
            },
        }),
        updateSighting: builder.mutation({
            query: ({ sightingId, info }) => {
                return {
                    url: `/api/sightings/${sightingId}`,
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(info),
                    credentials: 'include',
                }
            },
            invalidatesTags: (result) => {
                return (result && ['Sightings', 'Birds']) || []
            },
        }),
        deleteSighting: builder.mutation({
            query: ({ sightingId }) => {
                return {
                    url: `/api/sightings/${sightingId}`,
                    method: 'DELETE',
                    credentials: 'include',
                }
            },
            invalidatesTags: ['Sightings', 'Birds'],
        }),
        addImage: builder.mutation({
            query: ({ imageInput, image_id }) => {
                const formData = new FormData()
                formData.append('image', imageInput)
                return {
                    url: '/api/images',
                    method: 'POST',
                    params: {
                        image_id,
                    },
                    body: formData,
                    credentials: 'include',
                }
            },
        }),
    }),
})

export const {
    useGetBirdsQuery,
    useGetSightingsQuery,
    useLogoutMutation,
    useLoginMutation,
    useGetTokenQuery,
    useSignupMutation,
    useAddSightingMutation,
    useUpdateSightingMutation,
    useDeleteSightingMutation,
    useLazyGetSightingsQuery,
    useLazyGetObservationsQuery,
    useAddImageMutation,
} = birdsApi

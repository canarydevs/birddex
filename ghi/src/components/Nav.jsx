import { NavLink } from 'react-router-dom'
import { useGetTokenQuery, useLogoutMutation } from './api/birddexApi'
import { useNavigate } from 'react-router-dom'
import { setSelectedCard } from './selectedCard/selectedCardSlice'
import { setListInitialState } from './listPosition/listPositionSlice'
import { useDispatch } from 'react-redux'
import ContactForm from './ContactForm'

function Nav() {
    const navigate = useNavigate()
    const dispatch = useDispatch()
    // fetch logged in user
    const { data: user } = useGetTokenQuery()
    // logout mutation
    const [logout] = useLogoutMutation()

    // Logout the current user, reset states, and return to Landing
    const handleLogout = async () => {
        await logout()
        dispatch(setSelectedCard(-1))
        dispatch(setListInitialState())
        navigate('/')
    }

    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
            <div className="container-fluid">
                <img
                    src="\birddex-logo.png"
                    alt="Birddex Logo"
                    height={50}
                    width={50}
                />
                <NavLink className="navbar-brand" to="/">
                    <h2 className="mx-1">BirdDex</h2>
                </NavLink>
                <button
                    className="navbar-toggler"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div
                    className="collapse navbar-collapse"
                    id="navbarSupportedContent"
                >
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        {user && (
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/dashboard/">
                                    Dashboard
                                </NavLink>
                            </li>
                        )}
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/birds/">
                                Bird List
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <ContactForm/>
                        </li>
                    </ul>
                    {user && (
                        <button
                            className="btn btn-outline-danger px-2 py-2"
                            onClick={handleLogout}
                        >
                            Logout
                        </button>
                    )}
                </div>
            </div>
        </nav>
    )
}

export default Nav

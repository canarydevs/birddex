import { createSlice } from '@reduxjs/toolkit'

export const selectedCardSlice = createSlice({
    name: 'selectedCard',
    initialState: {
        value: {
            id: -1,
        },
    },
    reducers: {
        setSelectedCard: (state, action) => {
            state.value.id = action.payload
        },
        setSelectedInitialState: (state) => {
            state.value.id = -1
        },
    },
})

export const { setSelectedCard, setSelectedInitialState } =
    selectedCardSlice.actions

export default selectedCardSlice.reducer

import { createSlice } from '@reduxjs/toolkit'

export const listPositionSlice = createSlice({
    name: 'listPosition',
    initialState: {
        value: {
            listItems: {
                sightings: [],
                bird: {
                    image: '',
                    common_name: '',
                }},
            row: -1,
        },
    },
    reducers: {
        setListPosition: (state, action) => {
            state.value.row = action.payload
        },
        setListItems: (state, action) => {
            state.value.listItems = action.payload
        },
        setListInitialState: (state) => {
            state.value.listItems = {
                sightings: [],
                bird: {
                    image: '',
                    common_name: '',
                }}
            state.value.row = -1
        },
    },
})

export const {
    setListPosition,
    setListItems,
    setListInitialState,
} = listPositionSlice.actions

export default listPositionSlice.reducer

import { BrowserRouter, Routes, Route } from 'react-router-dom'
import './styles/App.css'
import Nav from './components/Nav.jsx'
import Landing from './routes/Landing.jsx'
import Dashboard from './routes/Dashboard.jsx'
import Birds from './routes/Birds.jsx'

// All your environment variables in vite are in this object
console.table(import.meta.env)

// When using environment variables, you should do a check to see if
// they are defined or not and throw an appropriate error message
const API_HOST = import.meta.env.VITE_API_HOST
const basename = import.meta.env.VITE_PUBLIC_URL

if (!API_HOST) {
    throw new Error('VITE_API_HOST is not defined')
}
if (typeof basename === 'undefined') {
    throw new Error('VITE_PUBLIC_URL is not defined')
}

/**
 * This is an example of using JSDOC to define types for your component
 * @typedef {{module: number, week: number, day: number, min: number, hour: number}} LaunchInfo
 * @typedef {{launch_details: LaunchInfo, message?: string}} LaunchData
 *
 * @returns {React.ReactNode}
 */
function App() {
    return (
        <BrowserRouter basename={basename}>
            <Nav />
            <main className="container">
                <Routes>
                    <Route path="/" element={<Landing />} />
                    <Route path="/dashboard" element={<Dashboard />} />
                    <Route path="/birds" element={<Birds />} />
                </Routes>
            </main>
        </BrowserRouter>
    )
}

export default App

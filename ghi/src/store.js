import { configureStore } from '@reduxjs/toolkit'
import { setupListeners } from '@reduxjs/toolkit/query'
import { birdsApi } from './components/api/birddexApi.js'
import listPositionReducer from './components/listPosition/listPositionSlice.js'
import selectedCardReducer from './components/selectedCard/selectedCardSlice.js'

export const store = configureStore({
    reducer: {
        listPosition: listPositionReducer,
        selectedCard: selectedCardReducer,
        [birdsApi.reducerPath]: birdsApi.reducer,
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware().concat(birdsApi.middleware),
})

setupListeners(store.dispatch)

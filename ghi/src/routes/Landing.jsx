import React from 'react'
import { useEffect } from 'react'
import {
    useLoginMutation,
    useSignupMutation,
} from '../components/api/birddexApi.js'
import * as Components from '../styles/LandingComp.jsx'
import { useNavigate } from 'react-router-dom'

const defaultUserInfo = {
    first_name: '',
    last_name: '',
    email: '',
    password: '',
    location: '',
}

function Landing() {
    const navigate = useNavigate()
    // holds the state of the overlay
    const [signIn, toggle] = React.useState(true)
    // holds the state of the form data
    const [userInfo, setUserInfo] = React.useState(defaultUserInfo)
    // holds any active error message
    const [err, setErr] = React.useState(null)
    // RTK Query state
    const [login, loginStatus] = useLoginMutation()
    const [signup, signupStatus] = useSignupMutation()

    // on login success navigate, on error display the error
    useEffect(() => {
        if (loginStatus.isSuccess || signupStatus.isSuccess) {
            navigate('/dashboard')
        }
        if (loginStatus.isError) {
            setErr('Invalid login credentials')
        } else if (signupStatus.isError) {
            setErr('Invalid signup credentials')
        }
    }, [loginStatus, signupStatus, navigate])

    // changes the displayed OverlayComponent
    const reset = () => {
        toggle(!signIn)
        setUserInfo(defaultUserInfo)
        setErr(null)
    }

    // login or signup according to the form used
    const handleSubmit = (e) => {
        e.preventDefault()

        if (e.target.name === 'signInForm') {
            login({ email: userInfo.email, password: userInfo.password })
        } else {
            signup(userInfo)
        }
    }

    // update form data
    const handleChange = (e) => {
        setUserInfo({
            ...userInfo,
            [e.target.name]: e.target.value,
        })
    }

    return (
        <>
            <Components.CenteredContainer>
                <Components.Container>
                    <Components.SignUpContainer $signin={signIn}>
                        <Components.Form
                            onSubmit={handleSubmit}
                            name="signUpForm"
                        >
                            <Components.Title>Join Us</Components.Title>
                            {err ? (
                                <div
                                    className={
                                        'alert alert-danger alert-dismissible'
                                    }
                                    role="alert"
                                >
                                    {err}
                                </div>
                            ) : null}
                            <Components.Input
                                required
                                type="text"
                                placeholder="First Name"
                                name="first_name"
                                autoComplete="given-name"
                                value={userInfo.firstName}
                                onChange={handleChange}
                            />
                            <Components.Input
                                required
                                type="text"
                                placeholder="Last Name"
                                name="last_name"
                                autoComplete="family-name"
                                value={userInfo.lastName}
                                onChange={handleChange}
                            />
                            <Components.Input
                                required
                                type="location"
                                placeholder="Location"
                                name="location"
                                autoComplete="location"
                                value={userInfo.location}
                                onChange={handleChange}
                            />
                            <Components.Input
                                required
                                type="email"
                                placeholder="Email"
                                name="email"
                                autoComplete="email"
                                value={userInfo.email}
                                onChange={handleChange}
                            />
                            <Components.Input
                                required
                                type="password"
                                placeholder="Password"
                                name="password"
                                autoComplete="new-password"
                                value={userInfo.password}
                                onChange={handleChange}
                            />
                            <Components.Button>Sign Up</Components.Button>
                        </Components.Form>
                    </Components.SignUpContainer>

                    <Components.SignInContainer $signin={signIn}>
                        <Components.Form
                            onSubmit={handleSubmit}
                            name="signInForm"
                        >
                            <Components.Title>Sign in</Components.Title>
                            {err ? (
                                <div
                                    className={
                                        'alert alert-danger alert-dismissible'
                                    }
                                    role="alert"
                                >
                                    {err}
                                </div>
                            ) : null}
                            <Components.Input
                                required
                                type="email"
                                placeholder="Email"
                                name="email"
                                autoComplete="email"
                                value={userInfo.email}
                                onChange={handleChange}
                            />
                            <Components.Input
                                required
                                type="password"
                                placeholder="Password"
                                name="password"
                                autoComplete="current-password"
                                value={userInfo.password}
                                onChange={handleChange}
                            />
                            <Components.Button>Sign In</Components.Button>
                        </Components.Form>
                    </Components.SignInContainer>

                    <Components.OverlayContainer $signin={signIn}>
                        <Components.Overlay $signin={signIn}>
                            <Components.LeftOverlayPanel $signin={signIn}>
                                <Components.Title>
                                    Birds! Birds! Everywhere!
                                </Components.Title>
                                <Components.Paragraph>
                                    Back to birding
                                </Components.Paragraph>
                                <Components.GhostButton onClick={reset}>
                                    Sign In
                                </Components.GhostButton>
                            </Components.LeftOverlayPanel>

                            <Components.RightOverlayPanel $signin={signIn}>
                                <Components.Title>
                                    Start birding today!
                                </Components.Title>
                                <Components.Paragraph>
                                    Sign up and begin your birding journey with
                                    our growing community.
                                </Components.Paragraph>
                                <Components.GhostButton onClick={reset}>
                                    Sign Up
                                </Components.GhostButton>
                            </Components.RightOverlayPanel>
                        </Components.Overlay>
                    </Components.OverlayContainer>
                </Components.Container>
            </Components.CenteredContainer>
        </>
    )
}

export default Landing

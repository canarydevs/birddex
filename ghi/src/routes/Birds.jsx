import { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import Card from '../components/birds/Card'
import BirdsListAccordion from '../components/birds/Accordion'
import { useGetBirdsQuery } from '../components/api/birddexApi'
import useWindowDimensions from '../components/windowDimensions/windowDimensions'
import PaginatedRows from '../components/birds/PaginatedRows'
import {useDispatch} from 'react-redux'
import { setListInitialState } from '../components/listPosition/listPositionSlice'
import { setSelectedCard } from '../components/selectedCard/selectedCardSlice'


function BirdsPage() {
    const dispatch = useDispatch()
    // holds any active error message
    const [err, setErr] = useState(null)
    // tracks the current pagination state
    const [currentPage, setCurrentPage] = useState(0)
    // tracks the current order state
    const [orderSelection, setOrderSelection] = useState('common-order')
    // fetch Birds list
    const {
        data: birds,
        error: birdsError,
        isLoading: birdsLoading,
    } = useGetBirdsQuery()
    // tracks the position and content of the drop-down list
    const listSet = useSelector((state) => state.listPosition.value)
    // changes the width of the rows as the screen gets smaller
    const rowModulus = useWindowDimensions()
    // tracks the state of the filters
    const [filterSightings, setFilterSightings] = useState(false)
    const [filterSearch, setFilterSearch] = useState('')


    // Check for fetching errors
    useEffect(() => {
        if (!birdsLoading && birdsError) {
            setErr(birdsError)
        }
    }, [setErr, birdsError, birdsLoading])

    // cancels rendering if birds data is not missing
    if (birdsLoading) {
        return <div>Data Loading...</div>
    }

    // filters the birds list
    const filteredBirds = birds.filter((bird) => {
        let keep = true
        if (filterSightings) keep = keep && bird.sightings.length > 0
        if (filterSearch)
            keep =
                keep &&
                (bird.common_name
                    .toLowerCase()
                    .includes(filterSearch.toLowerCase()) ||
                    bird.sci_name
                        .toLowerCase()
                        .includes(filterSearch.toLowerCase()))
        return keep
    })
    // orders the birds list
    if (filteredBirds && filteredBirds.length > 0) {
        switch (orderSelection) {
            case 'date-order':
                filteredBirds.sort(({ sightings: a }, { sightings: b }) => {
                    let dateA = '0000-00-00'
                    let dateB = '0000-00-00'
                    for (const sighting of a) {
                        if (sighting.date > dateA) {
                            dateA = sighting.date
                        }
                    }
                    for (const sighting of b) {
                        if (sighting.date > dateB) {
                            dateB = sighting.date
                        }
                    }
                    return dateA < dateB ? 1 : dateA > dateB ? -1 : 0
                })
                break
            case 'most-order':
                filteredBirds.sort(({ sightings: a }, { sightings: b }) =>
                    a.length < b.length ? 1 : a.length > b.length ? -1 : 0
                )
                break
            case 'least-order':
                filteredBirds.sort(({ sightings: a }, { sightings: b }) =>
                    a.length < b.length ? -1 : a.length > b.length ? 1 : 0
                )
                break
            case 'common-order':
                filteredBirds.sort((a, b) =>
                    a.common_name
                        .toLowerCase()
                        .localeCompare(b.common_name.toLowerCase())
                )
                break
            case 'sci-order':
                filteredBirds.sort((a, b) =>
                    a.sci_name
                        .toLowerCase()
                        .localeCompare(b.sci_name.toLowerCase())
                )
                break
        }
    }

    // functions to handle the filter state changes
    const filterSightingsToggle = () => {
        setFilterSightings(!filterSightings)
        dispatch(setListInitialState())
        dispatch(setSelectedCard())
        setCurrentPage(0)
    }

    const filterSearchChange = (event) => {
        setFilterSearch(event.target.value)
        setCurrentPage(0)
        dispatch(setListInitialState())
        dispatch(setSelectedCard())
    }

    const handleOrderSelect = (e) => {
        setOrderSelection(e.target.value)
        setCurrentPage(0)
        dispatch(setListInitialState())
        dispatch(setSelectedCard())
    }

    // builds the rows of cards
    const rowBuilder = (filteredBirds, cardIndex, row) => {
        const newRow = []
        // create a card box for each place in the row,
        // and exit when the row length = rowModulus
        do {
            // if there are more filteredBirds, create a card, else add an empty box
            if (cardIndex < filteredBirds.length) {
                newRow.push(
                    <Card
                        key={filteredBirds[cardIndex].id}
                        rowNum={row}
                        info={filteredBirds[cardIndex]}
                    />
                )
            } else {
                newRow.push(
                    <div
                        key={row + '' + cardIndex}
                        className="mx-auto my-2"
                        style={{ width: '17.5rem', height: '20.625rem' }}
                    ></div>
                )
            }
            cardIndex++
        } while (cardIndex % rowModulus !== 0)

        return [
            cardIndex,
            <div className="container" key={'row' + row}>
                <div className="row">{newRow}</div>
                <div>
                    {listSet.row !== row ? null : (
                        <>
                            <BirdsListAccordion info={listSet.listItems} />
                        </>
                    )}
                </div>
            </div>
        ]
    }

    // call each rowbuilder as necessary
    let row = 0
    let rows = []
    let cardIndex = 0

    // while there are cards in the filteredList,
    // create rows of cards && lists
    while (cardIndex <= filteredBirds.length) {
        const [endOfRow, jsx] = rowBuilder(filteredBirds, cardIndex, row)
        rows.push(jsx)
        cardIndex = endOfRow

        row++
    }

    return (
        <>
            <div id="title">
                <h1>Birds List</h1>
            </div>
            <button
                onClick={() => filterSightingsToggle()}
                className={
                    filterSightings
                        ? 'btn btn-primary px-2 py-2 me-2'
                        : 'btn btn-success px-2 py-2 me-2'
                }
            >
                Filter Sightings
            </button>
            <input
                type="text"
                id="searchFilter"
                onChange={filterSearchChange}
                className="search-bar"
            />
            <select
                onChange={handleOrderSelect}
                value={orderSelection}
                required
                id="order"
                name="order"
                className="m-1 p-1"
            >
                Order By:
                <option value="common-order" key="common-order">
                    Common Name - A-Z
                </option>
                <option value="sci-order" key="sci-order">
                    Scientific Name - A-Z
                </option>
                <option value="date-order" key="date-order">
                    Recent sighting
                </option>
                <option value="most-order" key="most-order">
                    # sightings - Descending
                </option>
                <option value="least-order" key="least-order">
                    # sightings - Ascending
                </option>
            </select>

            <p>Results: {filteredBirds.length} birds</p>
            {err ? (
                <div
                    className={'alert alert-danger alert-dismissible'}
                    role="alert"
                >
                    {err}
                </div>
            ) : null}
            <PaginatedRows
                key={rows.length}
                rowsPerPage={3}
                rows={rows}
                currentPage={currentPage}
                setCurrentPage={setCurrentPage}
            />
        </>
    )
}

export default BirdsPage

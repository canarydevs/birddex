FROM python:3.10-bullseye

# Upgrades pip to the latest version
RUN python -m pip install --upgrade pip

# Sets the working directory inside the container where our app will run
WORKDIR /app

# Copy the top-level files and directories
COPY acl acl
COPY queries queries
COPY routers routers
COPY migrations migrations

COPY authenticator.py authenticator.py
COPY main.py main.py
COPY models.py models.py
COPY requirements.txt requirements.txt
COPY seeder.py seeder.py

# Installs all the python packages listed in our requirements.txt file
RUN python -m pip install -r requirements.txt

# This is the command that runs when the docker container starts
CMD python -m migrations up && uvicorn main:app --reload --host 0.0.0.0 --port ${PORT} --forwarded-allow-ips "*"

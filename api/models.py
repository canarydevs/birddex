from pydantic import BaseModel
from datetime import date
from jwtdown_fastapi.authentication import Token


# User classes
class UserIn(BaseModel):
    email: str
    password: str
    first_name: str
    last_name: str
    location: str


class UserOut(BaseModel):
    id: int
    email: str
    first_name: str
    last_name: str
    location: str
    lat: float
    lng: float


class UserOutWithPassword(UserOut):
    hash_password: str


class UserForm(BaseModel):
    username: str
    password: str


class UserToken(Token):
    user: UserOut


# Bird Classes
class BirdSighting(BaseModel):
    id: int
    date: date
    location: str
    lat: float
    lng: float
    image: str


class BirdIn(BaseModel):
    common_name: str
    sci_name: str
    conservation_status: str
    image: str


class BirdOut(BaseModel):
    id: int
    common_name: str
    sci_name: str
    conservation_status: str
    image: str
    sightings: list[BirdSighting] | None


# Sighting Classes
class SightingInBase(BaseModel):
    date: date
    location: str
    image: str
    bird_id: int


class SightingIn(SightingInBase):
    user_id: int


class SightingOut(BaseModel):
    id: int
    date: date
    location: str
    lat: float
    lng: float
    image: str
    user: UserOut
    bird: BirdOut


# Observation Classes
class Observation(BaseModel):
    common_name: str
    sci_name: str
    location: str
    image: str | None


# Blob Storage Classes
class ImageFieldOut(BaseModel):
    blob_name: str | None
    content_type: str | None


# Error Classes
class ApiError(BaseModel):
    detail: str


class DatabaseError(BaseModel):
    detail: str


class HttpError(BaseModel):
    detail: str

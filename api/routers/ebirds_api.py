# router.py
from typing import List
from authenticator import authenticator
from fastapi import (
    Depends,
    APIRouter,
)
from acl.ebirds import get_obs
from queries.birds import BirdsRepo
from models import Observation, ApiError

router = APIRouter()


# get random birds from the US
@router.get("/ebirds/region", response_model=List[Observation] | ApiError)
async def endpoint_get_obs_US() -> List[Observation] | ApiError:
    repo = BirdsRepo()
    return get_obs(repo)


# get random birds from nearby the user
@router.get("/ebirds/nearby", response_model=List[Observation] | ApiError)
async def endpoint_get_obs_nearby(
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> List[Observation] | ApiError:
    repo = BirdsRepo()

    # call get_obs with the user's lat and lng from the account_data
    data = get_obs(repo, (account_data["lat"], account_data["lng"]))
    return data

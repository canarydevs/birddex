from typing import Union, List
from fastapi import Depends, APIRouter, HTTPException, status
from queries.sightings import SightingsRepo
from models import (
    DatabaseError,
    SightingIn,
    SightingOut,
    SightingInBase,
)
from authenticator import authenticator

router = APIRouter()


# Create Sighting for user
@router.post("/api/sightings", response_model=SightingOut | DatabaseError)
async def endpoint_create_sighting(
    info: SightingInBase,
    repo: SightingsRepo = Depends(),
    account: dict = Depends(authenticator.get_current_account_data),
) -> SightingOut | DatabaseError:
    sighting = repo.create_sighting(
        SightingIn(user_id=account.get("id"), **info.dict())
    )
    if isinstance(sighting, DatabaseError):
        print(sighting.detail)
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=sighting.detail,
        )
    return sighting


# Get Sightings for user
@router.get(
    "/api/sightings",
    response_model=Union[List[SightingOut], SightingOut, None, DatabaseError],
)
async def endpoint_get_sightings(
    account: dict = Depends(authenticator.get_current_account_data),
    common_name: str | None = None,
    repo: SightingsRepo = Depends(),
) -> Union[List[SightingOut], SightingOut, DatabaseError, None]:
    if common_name:
        sightings = repo.get_sightings(
            account.get("id"), "birds.common_name", f"'{common_name}'"
        )
    else:
        sightings = repo.get_sightings(user_id=account.get("id"))
    return sightings


# Get sighting details
@router.get(
    "/api/sightings/{id}",
    response_model=Union[List[SightingOut], SightingOut, DatabaseError, None],
)
async def endpoint_get_sighting_details(
    id: int,
    account: dict = Depends(authenticator.get_current_account_data),
    repo: SightingsRepo = Depends(),
) -> Union[List[SightingOut], SightingOut, DatabaseError, None]:
    sighting = repo.get_sightings(user_id=account.get("id"), sightings_id=id)
    return sighting


# Update sighting details
@router.put("/api/sightings/{id}", response_model=SightingOut | DatabaseError)
async def endpoint_update_sighting_data(
    info: SightingInBase,
    id: int,
    account: dict = Depends(authenticator.get_current_account_data),
    repo: SightingsRepo = Depends(),
) -> SightingOut | DatabaseError:
    sighting = repo.update_sighting(account.get("id"), id, info)
    return sighting


# Delete sighting
@router.delete("/api/sightings/{id}", response_model=bool | DatabaseError)
async def endpoint_delete_sighting_data(
    id: int,
    repo: SightingsRepo = Depends(),
    account: dict = Depends(authenticator.get_current_account_data),
) -> Union[bool, DatabaseError]:
    return repo.delete_sighting(id)

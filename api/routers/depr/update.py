from typing import List
from fastapi import APIRouter, Depends
from queries.table import CounterTable
from authenticator import authenticator
from models import (
    BirdIn,
    BirdOut,
    DatabaseError,
    ImageFieldOut,
    SightingInBase,
    SightingOut,
    UserOut,
)
from queries.blob import BlobQueries
from queries.birds import BirdsRepo
from queries.sightings import SightingsRepo
from acl.image_url import get_image_from_url

router = APIRouter()


@router.put(
    "/api/sightings_update", response_model=List[SightingOut] | DatabaseError
)
async def update_sightings_images(
    sightings_repo: SightingsRepo = Depends(),
    blob_repo: BlobQueries = Depends(),
    account_data: UserOut = Depends(authenticator.get_current_account_data),
):
    sightings = sightings_repo.get_sightings()
    for sighting in sightings:
        if sighting.image[:4] == "http":
            image, content_type = get_image_from_url(sighting.image)
            blob = blob_repo.deposit_bird_image(
                sighting.location[:8].replace(" ", ""), image, content_type
            )
            if isinstance(blob, ImageFieldOut):
                sighting.image = blob.blob_name
            else:
                sighting.image = "None"
            sightings_repo.update_sighting(
                sighting.user.id,
                sighting.id,
                SightingInBase(
                    date=sighting.date,
                    location=sighting.location,
                    image=sighting.image,
                    bird_id=sighting.bird.id,
                ),
            )
    return sightings


@router.put("/api/birds_update", response_model=List[BirdOut] | DatabaseError)
async def update_birds_images(
    birds_repo: BirdsRepo = Depends(),
    blob_repo: BlobQueries = Depends(),
    account_data: UserOut = Depends(authenticator.get_current_account_data),
):
    birds = birds_repo.get_birds()
    for bird in birds:
        if bird.image[:4] == "http":
            image, content_type = get_image_from_url(bird.image)
            blob = blob_repo.deposit_bird_image(
                bird.common_name[:8].replace(" ", ""), image, content_type
            )
            if isinstance(blob, ImageFieldOut):
                bird.image = blob.blob_name
            else:
                bird.image = "None"
            birds_repo.update_bird(
                bird.id,
                BirdIn(
                    common_name=bird.common_name,
                    sci_name=bird.sci_name,
                    conservation_status=bird.conservation_status,
                    image=bird.image,
                ),
            )
    return birds


@router.put("/api/birds_update/{id}", response_model=BirdOut | DatabaseError)
async def update_bird_image(
    id: int = 0,
    bird_repo: BirdsRepo = Depends(),
    blob_repo: BlobQueries = Depends(),
    index_table: CounterTable = Depends(),
    account_data: UserOut = Depends(authenticator.get_current_account_data),
):
    id = index_table.increment()
    bird = bird_repo.get_birds(id=int(id))[0]

    if bird.image[:4] == "http":
        image, content_type = get_image_from_url(bird.image)
        blob = blob_repo.deposit_bird_image(
            bird.common_name[:8].replace(" ", ""), image, content_type
        )
        if isinstance(blob, ImageFieldOut):
            bird.image = blob.blob_name
        else:
            bird.image = "None"
        bird = bird_repo.update_bird(
            bird.id,
            BirdIn(
                common_name=bird.common_name,
                sci_name=bird.sci_name,
                conservation_status=bird.conservation_status,
                image=bird.image,
            ),
        )

    return bird

from fastapi import (
    APIRouter,
    Response,
    Depends,
    HTTPException,
    status,
    File,
    UploadFile,
)
from fastapi.responses import FileResponse
from io import BytesIO
from authenticator import authenticator
from queries.blob import BlobQueries
from models import DatabaseError, UserOut, ImageFieldOut

router = APIRouter()


@router.get("/api/images/{image_id}")
async def endpoint_get_image(image_id: str, repo: BlobQueries = Depends()):
    if image_id == "None":
        return FileResponse(
            "files/placeholder.jpg", media_type="image/jpeg"
        )
    image_data, content_type = repo.get_bird_image(
        image_id
    )
    if isinstance(image_data, DatabaseError):
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Image not found:" + image_data.detail,
        )
    image_io = BytesIO(image_data)

    # Return the image data with the appropriate content type
    return Response(
        image_io.read(), media_type=content_type
    )  # Adjust the media_type as needed


@router.post("/api/images", response_model=ImageFieldOut | DatabaseError)
async def endpoint_post_image(
    image_id: str,
    image: UploadFile = File(...),
    repo: BlobQueries = Depends(),
    account_data: UserOut = Depends(authenticator.get_current_account_data),
) -> ImageFieldOut | DatabaseError:
    image_data = await image.read()
    content_type = image.content_type.replace("/", "_")
    return repo.deposit_bird_image(image_id, image_data, content_type)

# router.py
from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from authenticator import authenticator

from queries.users import UsersRepo

from models import (
    DatabaseError,
    UserForm,
    UserToken,
    HttpError,
    UserIn,
    UserOut,
)

router = APIRouter()


@router.post(
    "/api/users", response_model=UserToken | HttpError | DatabaseError
)
async def endpoint_create_account(
    info: UserIn,
    request: Request,
    response: Response,
    repo: UsersRepo = Depends(),
):
    hash_password = authenticator.hash_password(info.password)
    try:
        user = repo.create(info, hash_password)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create an User with those credentials. "
            "Exception: " + str(e),
        )
    try:
        form = UserForm(username=info.email, password=info.password)
        token = await authenticator.login(response, request, form, repo)
        return UserToken(user=user, **token.dict())
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="Internal error: " + str(e),
        )


@router.get("/token", response_model=UserToken | DatabaseError | None)
async def get_token(
    request: Request,
    user: UserOut = Depends(authenticator.try_get_current_account_data),
) -> UserToken | None:
    if user and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "user": user,
        }

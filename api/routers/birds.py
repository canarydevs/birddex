# router.py
from typing import Union, List
from queries.sightings import SightingsRepo

from fastapi import (
    Depends,
    HTTPException,
    status,
    APIRouter,
)

from queries.birds import BirdsRepo
from authenticator import authenticator

from models import (
    BirdSighting,
    DatabaseError,
    UserOut,
    HttpError,
    BirdIn,
    BirdOut,
)

router = APIRouter()


# Create Bird
@router.post("/api/birds", response_model=BirdOut | HttpError | DatabaseError)
async def endpoint_create_bird(
    info: BirdIn,
    repo: BirdsRepo = Depends(),
) -> BirdOut | DatabaseError:
    try:
        bird = repo.create_bird(info)
    except (Exception, BaseException) as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Cannot create bird. Exception: {str(e)}",
        )
    return bird


# Get Bird(s) by common_name or sci_name via query params
#   No query params lists all birds
@router.get(
    "/api/birds",
    response_model=Union[List[BirdOut], BirdOut, None, DatabaseError],
)
async def endpoint_get_birds(
    user: UserOut = Depends(authenticator.try_get_current_account_data),
    repo: BirdsRepo = Depends(),
    sightingsRepo: SightingsRepo = Depends(),
    sci_name: str | None = None,
    common_name: str | None = None,
) -> Union[List[BirdOut], BirdOut, None, DatabaseError]:
    try:
        if common_name:
            birds = repo.get_birds(common_name=common_name)
        elif sci_name:
            birds = repo.get_birds(sci_name=sci_name)
        else:
            birds = repo.get_birds()
        if user:
            sightings = sightingsRepo.get_sightings(user_id=user["id"])
        else:
            sightings = []
        for bird in birds:
            bird.sightings = [
                BirdSighting(**sighting.dict())
                for sighting in sightings
                if sighting.bird.id == bird.id
            ]
    except (Exception, BaseException) as e:
        print(e)
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Cannot list bird. Exception: {str(e)}",
        )

    return birds


@router.put("/api/birds/{id}", response_model=BirdOut | HttpError)
async def endpoint_update_bird(
    id: int,
    info: BirdIn,
    repo: BirdsRepo = Depends(),
) -> BirdOut:
    try:
        bird = repo.update_bird(id, info)
    except (Exception, BaseException) as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Cannot update bird. Exception: {str(e)}",
        )
    return bird


# Populates Birds table with data pulled from Nuthatch
@router.get("/api/birds/populate", response_model=bool)
async def populate_table(
    repo: BirdsRepo = Depends(), page_number: int = 1
) -> bool:
    try:
        repo.populate_table(page_number)
    except Exception:
        return False

    return True

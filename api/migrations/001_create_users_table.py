steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE users (
            id SERIAL PRIMARY KEY NOT NULL,
            email VARCHAR(100) NOT NULL UNIQUE,
            hash_password VARCHAR(100) NOT NULL,
            first_name VARCHAR(100) NOT NULL,
            last_name VARCHAR(100) NOT NULL,
            location VARCHAR(100) NOT NULL,
            lat FLOAT NOT NULL,
            lng FLOAT NOT NULL
        )
        """,
        # "Down" SQL statement
        """
        DROP TABLE users;
        """
    ]
]

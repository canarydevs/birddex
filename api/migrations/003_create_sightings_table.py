steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE sightings (
            id SERIAL PRIMARY KEY NOT NULL,
            date DATE NOT NULL,
            location VARCHAR(100) NOT NULL,
            lat FLOAT NOT NULL,
            lng FLOAT NOT NULL,
            image VARCHAR(200) NOT NULL,
            bird_id INTEGER REFERENCES birds(id) NOT NULL,
            user_id INTEGER REFERENCES users(id) NOT NULL
        )
        """,
        # "Down" SQL statement
        """
        DROP TABLE sightings;
        """,
    ]
]

steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE birds (
            id SERIAL PRIMARY KEY NOT NULL,
            common_name VARCHAR(100) UNIQUE NOT NULL,
            sci_name VARCHAR(100) NOT NULL,
            conservation_status VARCHAR(100) NOT NULL,
            image VARCHAR(200) NOT NULL
        )
        """,
        # "Down" SQL statement
        """
        DROP TABLE birds;
        """,
    ]
]

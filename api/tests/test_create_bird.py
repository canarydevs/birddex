from fastapi.testclient import TestClient
from main import app
from queries.birds import BirdsRepo
from models import BirdOut, BirdIn

client = TestClient(app)


class FakeBirdsRepo:
    def create_bird(self, info: BirdIn) -> BirdOut:
        bird = BirdOut(
            id=1,
            common_name=info.common_name,
            sci_name=info.sci_name,
            conservation_status=info.conservation_status,
            image=info.image,
        )

        return bird


def test_create_bird():
    app.dependency_overrides[BirdsRepo] = FakeBirdsRepo

    bird = {
        "id": 1,
        "common_name": "Black-bellied Whistling-Duck",
        "sci_name": "Dendrocygna autumnalis",
        "conservation_status": "Low Concern",
        "image": "https://images.unsplash.com/photo-1542252223-c7f5b1142f93",
        "sightings": None,
    }

    # Act
    response = client.post("/api/birds", json=bird)

    # Assert
    assert response.status_code == 200
    assert response.json() == bird

    # Clean up
    app.dependency_overrides = {}

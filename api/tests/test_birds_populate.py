from fastapi.testclient import TestClient
from main import app
from queries.birds import BirdsRepo

client = TestClient(app)


class FakeNuthatch:
    def populate_table(self, page_num: int = None) -> bool:
        return True


def test_populate_table():
    # Arrange
    app.dependency_overrides[BirdsRepo] = FakeNuthatch

    # Act
    response = client.get("/api/birds/populate")

    # Assert
    assert response.status_code == 200
    assert response.content

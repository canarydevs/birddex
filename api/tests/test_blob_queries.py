import unittest
from unittest.mock import MagicMock, patch
from models import ImageFieldOut
from queries.blob import BlobQueries
import io
from PIL import Image


class TestBlobRepo(unittest.TestCase):
    @patch("azure.storage.blob.BlobServiceClient.from_connection_string")
    def test_list_blobs(self, mock_client_from):
        # Arrange
        fake_db = [
            {"name": "image1.jpg", "content_type": "image/jpeg"},
            {"name": "image2.jpg", "content_type": "image/jpeg"},
        ]

        #   Mock the container interface
        mock_container = MagicMock()
        mock_container.list_blobs.return_value = fake_db

        #   Mock the service client interface
        mock_service_client = MagicMock()
        mock_service_client.get_container_client.return_value = mock_container

        #   Mock the storage connection
        mock_client_from.return_value = mock_service_client

        #  testing source and expected result
        blob_queries = BlobQueries()
        expected_result = ["image1.jpg", "image2.jpg"]

        # Act
        result = blob_queries.list_blobs()

        # Assert
        self.assertEqual(result, expected_result)

        # Clean up

    @patch("azure.storage.blob.BlobServiceClient.from_connection_string")
    def test_get_bird_image(self, mock_client_from):
        # Arrange
        fake_image = b"image1"

        #  Mock the blob interface
        mock_blob = MagicMock()
        mock_blob.download_blob.return_value.readall.return_value = fake_image

        #   Mock the container interface
        mock_container = MagicMock()
        mock_container.get_blob_client.return_value = mock_blob

        #   Mock the service client interface
        mock_service_client = MagicMock()
        mock_service_client.get_container_client.return_value = mock_container

        #   Mock the storage connection
        mock_client_from.return_value = mock_service_client

        #   testing source and expected result
        blob_queries = BlobQueries()
        expected_result = (
            b"image1",
            "image/jpeg",
        )

        # Act
        result = blob_queries.get_bird_image("image1&image_jpeg&XXX")

        # Assert
        self.assertEqual(result, expected_result)

        # Clean Up

    @patch("azure.storage.blob.BlobServiceClient.from_connection_string")
    def test_get_bird_image_missing(self, mock_client_from):
        # Arrange
        fake_image = None

        #  Mock the blob interface
        mock_blob = MagicMock()
        mock_blob.download_blob.return_value.readall.return_value = fake_image

        #   Mock the container interface
        mock_container = MagicMock()
        mock_container.get_blob_client.return_value = mock_blob

        #   Mock the service client interface
        mock_service_client = MagicMock()
        mock_service_client.get_container_client.return_value = mock_container

        #   Mock the storage connection
        mock_client_from.return_value = mock_service_client

        #   testing source and expected result
        blob_queries = BlobQueries()

        # Act
        result = blob_queries.get_bird_image("image1&image_jpeg&XXX")

        # Assert
        self.assertEqual(result[0], None)
        self.assertEqual(result[1], "image/jpeg")

        # Clean Up

    @patch("azure.storage.blob.BlobServiceClient.from_connection_string")
    @patch("queries.blob.image_refine")
    def test_deposit_bird_image(self, mock_image_refine, mock_client_from):
        # Arrange
        #   Create an dummy image
        image = Image.new('RGB', (60, 30), color='red')
        byte_arr = io.BytesIO()
        image.save(byte_arr, format='JPEG')
        fake_image = byte_arr.getvalue()

        #   Mock the blob interface
        mock_blob = MagicMock()

        #   Mock the container interface
        mock_container = MagicMock()
        mock_container.get_blob_client.return_value = mock_blob

        #   Mock the service client interface
        mock_service_client = MagicMock()
        mock_service_client.get_container_client.return_value = mock_container

        #   Mock the storage connection and the refining function
        mock_client_from.return_value = mock_service_client
        mock_image_refine.return_value = fake_image

        #   testing source and expected result
        blob_queries = BlobQueries()

        # Act
        result = blob_queries.deposit_bird_image(
            "image1",
            fake_image,
            "image_jpeg"
        )

        # Assert
        self.assertIsInstance(result, ImageFieldOut)
        self.assertEqual(result.blob_name.split('&')[0], "image1")
        mock_blob.upload_blob.assert_called_once_with(
            fake_image,
            overwrite=True
        )

        # Clean Up

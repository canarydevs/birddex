from fastapi.testclient import TestClient
from models import Observation
from authenticator import authenticator
from main import app
from unittest.mock import patch
import unittest

client = TestClient(app)


def fake_get_current_account_data():
    return {
        "id": 1,
        "email": "beowulf@geats.com",
        "first_name": "Emily",
        "last_name": "Johnson",
        "location": "Miami, FL",
        "lat": 37,
        "lng": -97,
    }


class FakeRepo:
    pass


class TestObsEndpoint(unittest.TestCase):
    @patch("routers.ebirds_api.get_obs")
    @patch("routers.ebirds_api.BirdsRepo")
    def test_get_obs_nearby(self, mock_repo, mock_get_obs):
        # Arrange
        fake_repo = FakeRepo()
        mock_repo.return_value = fake_repo
        mock_get_obs.return_value = [
            Observation(
                common_name="American Crow",
                sci_name="Turdus migratorius",
                location="Miami, FL",
                image="example.com/image.jpg",
            ),
            Observation(
                common_name="American Robin",
                sci_name="Turdus migratorius",
                location="Miami, FL",
                image="example.com/image.jpg",
            ),
            Observation(
                common_name="American Bird",
                sci_name="Turdus migratorius",
                location="Miami, FL",
                image="example.com/image.jpg",
            ),
        ]
        app.dependency_overrides[
            authenticator.get_current_account_data
        ] = fake_get_current_account_data

        # Act
        response = client.get("/ebirds/nearby")

        # Assert
        assert response.status_code == 200
        mock_get_obs.assert_called_once_with(
            fake_repo, (37, -97)
        )

        # Clean up
        app.dependency_overrides = {}

    @patch("routers.ebirds_api.get_obs")
    @patch("routers.ebirds_api.BirdsRepo")
    def test_get_obs_region(self, mock_repo, mock_get_obs):
        # Arrange
        mock_repo.return_value = FakeRepo()
        mock_get_obs.return_value = [
            Observation(
                common_name="American Crow",
                sci_name="Turdus migratorius",
                location="Miami, FL",
                image="example.com/image.jpg",
            ),
            Observation(
                common_name="American Robin",
                sci_name="Turdus migratorius",
                location="Miami, FL",
                image="example.com/image.jpg",
            ),
            Observation(
                common_name="American Bird",
                sci_name="Turdus migratorius",
                location="Miami, FL",
                image="example.com/image.jpg",
            ),
        ]

        # Act
        response = client.get("/ebirds/region")

        # Assert
        assert response.status_code == 200
        assert len(response.json()) == 3

        # Clean up
        app.dependency_overrides = {}

from fastapi.testclient import TestClient
from main import app

from queries.sightings import SightingsRepo
from authenticator import authenticator

from models import (
    UserOut,
    BirdOut,
    SightingOut,
)

client = TestClient(app)


class FakeSightingsRepo:
    def get_sightings(
        self, user_id: int, common_name=None, sci_name=None, sightings_id=None
    ) -> SightingOut:
        sightings = [
            SightingOut(
                id=1,
                date="2024-01-22",
                location="Alice Wainwright Park",
                lat=25.7483317,
                lng=-80.2064008,
                image="https://images.unsplash.com/"
                "photo-1677088441517-4ee9e91d84a2",
                user=UserOut(
                    id=1,
                    email="user1@example.com",
                    first_name="Emily",
                    last_name="Johnson",
                    location="Miami, FL",
                    lat=25.7616798,
                    lng=-80.1917902,
                ),
                bird=BirdOut(
                    id=141,
                    common_name="White Ibis",
                    sci_name="Eudocimus albus",
                    conservation_status="Low Concern",
                    image="https://images.unsplash.com/"
                    "photo-1502202758319-377d4fe4b6fa",
                ),
            )
        ]
        return sightings


def fake_get_account_data():
    return {
        "id": 1,
        "email": "",
        "first_name": "",
        "last_name": "",
        "location": "",
        "lat": 0,
        "lng": 0,
    }


def test_sightings_detail():
    # Arrange
    app.dependency_overrides[SightingsRepo] = FakeSightingsRepo
    app.dependency_overrides[authenticator.get_current_account_data] = (
        fake_get_account_data
    )

    list_of_sightings = [
        {
            "id": 1,
            "date": "2024-01-22",
            "location": "Alice Wainwright Park",
            "lat": 25.7483317,
            "lng": -80.2064008,
            "image": "https://images.unsplash.com/"
            "photo-1677088441517-4ee9e91d84a2",
            "user": {
                "id": 1,
                "email": "user1@example.com",
                "first_name": "Emily",
                "last_name": "Johnson",
                "location": "Miami, FL",
                "lat": 25.7616798,
                "lng": -80.1917902,
            },
            "bird": {
                "id": 141,
                "common_name": "White Ibis",
                "sci_name": "Eudocimus albus",
                "conservation_status": "Low Concern",
                "image": "https://images.unsplash.com/"
                "photo-1502202758319-377d4fe4b6fa",
                "sightings": None,
            },
        }
    ]

    # Act
    response = client.get("/api/sightings/1")

    # Assert
    assert response.status_code == 200
    assert response.json() == list_of_sightings

    # Clean up
    app.dependency_overrides = {}

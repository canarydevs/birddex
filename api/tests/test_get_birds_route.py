from typing import List
from fastapi.testclient import TestClient
from main import app
from queries.birds import BirdsRepo
from queries.sightings import SightingsRepo
from models import BirdOut, DatabaseError, SightingOut, UserOut
from authenticator import authenticator

client = TestClient(app)


class FakeBirds:
    def get_birds(self, common_name=None, sci_name=None):
        return [
            BirdOut(
                id=1,
                common_name="Golduck",
                sci_name="BigBlueDuck",
                conservation_status="LC",
                image="https://assets.pokemon.com/assets/"
                "cms2/img/pokedex/full/055.png",
            )
        ]


class FakeSightings:
    def get_sightings(
        self,
        user_id: int = None,
        sci_name: str = None,
        common_name: str = None,
        sighting_id: int = None,
    ) -> List[SightingOut] | DatabaseError:
        return SightingOut(
            id=1,
            date="2021-01-01",
            location="New York",
            lat=40.7128,
            lng=-74.0060,
            image="https://assets.pokemon.com/assets/"
            "cms2/img/pokedex/full/055.png",
            user=UserOut(
                id=1,
                email="new@email.com",
                first_name="New",
                last_name="User",
                location="New York",
                lat=40.7128,
                lng=-74.0060,
            ),
            bird=BirdOut(
                id=1,
                common_name="Golduck",
                sci_name="BigBlueDuck",
                conservation_status="LC",
                image="https://assets.pokemon.com/assets/"
                "cms2/img/pokedex/full/055.png",
            ),
        )


def fake_get_current_account_data():
    return {
        "id": 1,
        "email": "beowulf@geats.com",
        "first_name": "Emily",
        "last_name": "Johnson",
        "location": "Miami, FL",
        "lat": 0,
        "lng": 0,
    }


def test_get_api_birds():
    # Arrange
    app.dependency_overrides[BirdsRepo] = FakeBirds
    app.dependency_overrides[SightingsRepo] = FakeSightings
    app.dependency_overrides[authenticator.get_current_account_data] = (
        fake_get_current_account_data
    )

    # Act
    response = client.get("/api/birds")

    # Assert
    assert response.status_code == 200
    assert "sightings" in response.json()[0].keys()

    # Clean
    app.dependency_overrides = {}

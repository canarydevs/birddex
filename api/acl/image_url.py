from typing import Tuple
import requests
from models import ApiError


def get_image_from_url(url: str) -> Tuple[bytes, str] | ApiError:
    """
    Get image data from a URL
    """
    FORTY_MB = 41943040
    try:
        response = requests.get(url)
        print(
            len(response.content) / 1000000
        ) if len(response.content) > FORTY_MB else None
        if response.status_code == 200 and len(response.content) < FORTY_MB:
            image = response.content
            content_type = response.headers.get("Content-Type").replace(
                "/", "_"
            )
            return image, content_type
        return bytes(0), "image_jpeg"
    except Exception as e:
        print(str(e))
        return ApiError(detail="Could not connect to image URL: " + str(e))

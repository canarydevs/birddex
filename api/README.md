## For backend Heroku deployment

From `birddex/api` folder:

#### Build an image
```bash
docker build . -t birddex-api
```
#### Push the image
```bash
heroku container:push web -a birddex
```
#### Release the image
```bash
heroku container:release web --app birddex
```
#### Connect to the logs to monitor
```bash
heroku logs --tail --app birddex
```

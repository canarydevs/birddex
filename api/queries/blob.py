import uuid
from azure.storage.blob import BlobServiceClient
import os
from io import BytesIO
from typing import List
from models import DatabaseError, ImageFieldOut
from PIL import Image

BLOB_CONNECTION_STRING = os.environ.get("BLOB_CONNECTION_STRING")
if BLOB_CONNECTION_STRING is None:
    raise ValueError("No BLOB_CONNECTION_STRING is set")

BIRDS_CONTAINER_NAME = os.environ.get("BLOB_CONTAINER_NAME")
if BIRDS_CONTAINER_NAME is None:
    raise ValueError("No BLOB_CONTAINER_NAME is set")


class BlobQueries:
    def list_blobs(self) -> List[str] | DatabaseError:
        try:
            blob_service_client = BlobServiceClient.from_connection_string(
                BLOB_CONNECTION_STRING
            )

            container_client = blob_service_client.get_container_client(
                BIRDS_CONTAINER_NAME
            )
            blob_list = container_client.list_blobs()
            return [blob.get("name") for blob in blob_list]
        except Exception as e:
            return (
                DatabaseError(
                    detail="Failed to connect to birddex-images: " + str(e)
                ),
                None,
            )

    def get_bird_image(self, blob_name: str) -> bytes | DatabaseError:
        try:
            blob_service_client = BlobServiceClient.from_connection_string(
                BLOB_CONNECTION_STRING
            )

            container_client = blob_service_client.get_container_client(
                BIRDS_CONTAINER_NAME
            )
            blob_client = container_client.get_blob_client(blob_name)
            content_type = blob_name.split("&")[1].replace("_", "/")
            return blob_client.download_blob().readall(), content_type
        except Exception as e:
            return (
                DatabaseError(
                    detail="Failed to connect to birddex-images: " + str(e)
                ),
                None,
            )

    def deposit_bird_image(
        self, source_name: str, source: bytes, content_type: str
    ) -> ImageFieldOut | DatabaseError:
        try:
            source = image_refine(source, content_type)
        except Exception as e:
            print("Failed to refine image: " + str(e))
            return ImageFieldOut(blob_name="None", content_type="image_jpeg")
        try:
            blob_service_client = BlobServiceClient.from_connection_string(
                BLOB_CONNECTION_STRING
            )

            container_client = blob_service_client.get_container_client(
                BIRDS_CONTAINER_NAME
            )

            try:
                # blob name is { given&content_type&uuid }
                blob_name = (
                    f"{source_name}&" f"{content_type}&" f"{uuid.uuid4()}"
                )

                blob_client = container_client.get_blob_client(blob_name)
                blob_client.upload_blob(source, overwrite=True)
                return ImageFieldOut(
                    blob_name=blob_name, content_type=content_type
                )
            except Exception as e:
                return DatabaseError(
                    detail="Failed to connect to birddex-images: " + str(e)
                )
        except Exception as e:
            return DatabaseError(
                detail="Failed to connect to birddex-images: " + str(e)
            )


def image_refine(source: bytes, content_type: str) -> bytes:

    ext = content_type.split("_")[-1]
    image = Image.open(BytesIO(source))
    left, upper, right, lower = image.getbbox()
    ratio = (right - left) / (lower - upper)
    if ratio * 4 > 3:
        width = right - left
        new_width = (lower - upper) * 4 / 3
        image = image.crop(
            (
                int((width - new_width) // 2),
                upper,
                int((width - new_width) // 2) + int(new_width),
                lower,
            )
        )
    elif ratio * 4 < 3:
        height = lower - upper
        new_height = (right - left) * 3 / 4
        image = image.crop(
            (
                left,
                int((height - new_height) // 2),
                right,
                int((height - new_height) // 2) + int(new_height),
            )
        )
    if image.width > 640 or image.height > 480:
        image = image.resize((640, 480))
    # Save the image with the correct file extension
    with BytesIO() as output:
        image.save(output, format=ext.upper())
        output.seek(0)
        return output.read()

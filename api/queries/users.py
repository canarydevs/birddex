from models import UserOutWithPassword, UserIn, DatabaseError
from queries.pool import pool
from acl.geocode import get_location


class UsersRepo:

    def get(self, email: str) -> UserOutWithPassword | DatabaseError:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM users
                        WHERE email = %s
                        """,
                        [email],
                    )
                    data = db.fetchone()
                    user = UserOutWithPassword(
                        id=data[0],
                        email=data[1],
                        hash_password=data[2],
                        first_name=data[3],
                        last_name=data[4],
                        location=data[5],
                        lat=data[6],
                        lng=data[7],
                    )
                    return user

        except Exception as e:
            return DatabaseError(detail="Error accessing database: " + str(e))

    def create(
        self, info: UserIn, hash_password: str
    ) -> UserOutWithPassword | DatabaseError:
        address = get_location(info.location)
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO users
                            (email, hash_password, first_name,
                             last_name, location, lat, lng)
                        VALUES (%s, %s, %s, %s, %s, %s, %s)
                        RETURNING id
                        """,
                        [
                            info.email,
                            hash_password,
                            info.first_name,
                            info.last_name,
                            info.location,
                            address[0],
                            address[1],
                        ],
                    )
                    id = db.fetchone()[0]
                    return UserOutWithPassword(
                        id=id,
                        email=info.email,
                        hash_password=hash_password,
                        first_name=info.first_name,
                        last_name=info.last_name,
                        location=info.location,
                        lat=address[0],
                        lng=address[1],
                    )
        except Exception as e:
            return DatabaseError(detail="Error accessing database: " + str(e))

from typing import List
from models import BirdIn, BirdOut, DatabaseError
from queries.pool import pool
from acl import nuthatch


class BirdsRepo:
    def get_birds(
        self, common_name: str = None, sci_name: str = None, id: int = None
    ) -> List[BirdOut] | DatabaseError:
        sql = "SELECT * FROM birds"
        args = []
        if id:
            sql += " WHERE id = %s"
            args.append(id)
        elif common_name:
            sql += " WHERE common_name = %s"
            args.append(common_name)
        elif sci_name:
            sql += " WHERE sci_name = %s"
            args.append(sci_name)
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        sql,
                        args
                    )
                    dataset = db.fetchall()
                    birds = [
                        BirdOut(
                            id=data[0],
                            common_name=data[1],
                            sci_name=data[2],
                            conservation_status=data[3],
                            image=data[4],
                        )
                        for data in dataset
                    ]
                    return birds
        except Exception as e:
            return DatabaseError(detail="Error accessing database: " + str(e))

    def create_bird(self, info: BirdIn) -> BirdOut | DatabaseError:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO birds
                            (common_name, sci_name, conservation_status, image)
                        VALUES (%s, %s, %s, %s)
                        ON CONFLICT (common_name) DO NOTHING
                        RETURNING
                            id,
                            common_name,
                            sci_name,
                            conservation_status,
                            image
                        """,
                        [
                            info.common_name,
                            info.sci_name,
                            info.conservation_status,
                            info.image,
                        ],
                    )
                    id = db.fetchone()[0]
                    return BirdOut(
                        id=id,
                        common_name=info.common_name,
                        sci_name=info.sci_name,
                        conservation_status=info.conservation_status,
                        image=info.image,
                    )
        except Exception as e:
            return DatabaseError(detail="Error accessing database: " + str(e))

    def update_bird(self, id: int, info: BirdIn) -> BirdOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE birds
                        SET
                            common_name = %s,
                            sci_name = %s,
                            conservation_status = %s,
                            image = %s
                        WHERE id = %s
                        RETURNING
                            id,
                            common_name,
                            sci_name,
                            conservation_status,
                            image
                        """,
                        [
                            info.common_name,
                            info.sci_name,
                            info.conservation_status,
                            info.image,
                            id,
                        ],
                    )
                    data = db.fetchone()
                    return BirdOut(
                        id=data[0],
                        common_name=data[1],
                        sci_name=data[2],
                        conservation_status=data[3],
                        image=data[4],
                    )
        except Exception as e:
            return DatabaseError(detail="Error accessing database: " + str(e))

    def populate_table(self, page_number: int = 1) -> bool:
        # get a list of birds and add each to the database
        birds = nuthatch.get_birds(page_number)

        for bird in birds:
            self.create_bird(bird)

        return True

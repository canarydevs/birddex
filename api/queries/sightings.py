from typing import List
from models import (
    SightingIn,
    SightingInBase,
    SightingOut,
    BirdOut,
    UserOut,
    DatabaseError,
)
from queries.pool import pool
from acl.geocode import get_location


class SightingsRepo:
    def create_sighting(self, info: SightingIn) -> SightingOut | DatabaseError:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    lat, lng = get_location(info.location)
                    conn.autocommit = False
                    db.execute(
                        """
                        INSERT INTO sightings
                            (date, location, lat, lng, image, user_id, bird_id)
                        VALUES (%s, %s, %s, %s, %s, %s, %s)
                        RETURNING
                            id
                        """,
                        [
                            info.date,
                            info.location,
                            lat,
                            lng,
                            info.image,
                            info.user_id,
                            info.bird_id,
                        ],
                    )
                    conn.commit()
                    sighting_data = db.fetchone()
                    data = self.get_sightings(
                        sighting_id=sighting_data[0]
                    )[0].dict()
                    sighting = SightingOut(**data)
                    return sighting

        except Exception as e:
            return DatabaseError(detail="Error accessing database: " + str(e))
        finally:
            conn.autocommit = True

    def get_sightings(
        self,
        user_id: int = None,
        sci_name: str = None,
        common_name: str = None,
        sighting_id: int = None,
    ) -> List[SightingOut] | DatabaseError:
        sql = []
        args = []
        if user_id:
            sql.append("users.id = %s")
            args.append(user_id)
        if sighting_id:
            sql.append("sightings.id = %s")
            args.append(sighting_id)
        if common_name:
            sql.append("birds.common_name = %s")
            args.append(common_name)
        elif sci_name:
            sql.append("birds.sci_name = %s")
            args.append(sci_name)
        if sql:
            if len(sql) > 1:
                sql = "WHERE " + " AND ".join(sql)
            else:
                sql = "WHERE " + sql[0]
        else:
            sql = ""
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                            sightings.id,
                            sightings.date,
                            sightings.location,
                            sightings.lat,
                            sightings.lng,
                            sightings.image,
                            users.id,
                            users.email,
                            users.first_name,
                            users.last_name,
                            users.location,
                            users.lat,
                            users.lng,
                            birds.id,
                            birds.common_name,
                            birds.sci_name,
                            birds.conservation_status,
                            birds.image
                        FROM
                            sightings
                        JOIN
                            birds ON sightings.bird_id = birds.id
                        JOIN
                            users ON sightings.user_id = users.id
                        {};
                        """.format(
                            sql
                        ),
                        args,
                    )
                    dataset = db.fetchall()
                    sightings = [
                        SightingOut(
                            id=data[0],
                            date=data[1],
                            location=data[2],
                            lat=data[3],
                            lng=data[4],
                            image=data[5],
                            user=UserOut(
                                id=data[6],
                                email=data[7],
                                first_name=data[8],
                                last_name=data[9],
                                location=data[10],
                                lat=data[11],
                                lng=data[12],
                            ),
                            bird=BirdOut(
                                id=data[13],
                                common_name=data[14],
                                sci_name=data[15],
                                conservation_status=data[16],
                                image=data[17],
                            ),
                        )
                        for data in dataset
                    ]
                    return sightings
        except Exception as e:
            return DatabaseError(detail="Error accessing database: " + str(e))

    def update_sighting(
        self, user_id: int, id: int, info: SightingInBase
    ) -> SightingOut | DatabaseError:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    lat, lng = get_location(info.location)
                    conn.autocommit = False
                    db.execute(
                        """
                        UPDATE sightings
                        SET
                            date = %s,
                            location = %s,
                            lat = %s,
                            lng = %s,
                            image = %s,
                            bird_id = %s
                        WHERE sightings.id = %s
                        """,
                        [
                            info.date,
                            info.location,
                            lat,
                            lng,
                            info.image,
                            info.bird_id,
                            id,
                        ],
                    )
                    conn.commit()
                    data = self.get_sightings(user_id, sighting_id=id)[
                        0
                    ].dict()
                    sighting = SightingOut(**data)
                    return sighting

        except Exception as e:
            return DatabaseError(detail="Error accessing database: " + str(e))
        finally:
            conn.autocommit = True

    def delete_sighting(self, id: int) -> bool | DatabaseError:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM sightings
                        WHERE id = %s;
                        """,
                        [id],
                    )
                    return True

        except Exception as e:
            return DatabaseError(detail="Error accessing database: " + str(e))
